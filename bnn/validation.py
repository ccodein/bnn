"""This module contains definitions and functions for data validation."""
from bnn import exceptions as bnn_ex
from typing import Union, overload, Any, Tuple, List, TypedDict, Callable
from datetime import date, datetime
from bnn import mapper


class DefinitionType(TypedDict):
    """
    The type for the definiton dict.

    The definition dictionary describes the respective properties of the Bnn3
    and the Product3 data classes. The definition is used for validation.
    """

    name: str
    """The name of the property."""

    max_length: Union[int, Tuple[int, int]]
    """The maximum length if the string representation of the data type."""

    options: Union[None, str, tuple]
    """The possible options of the value."""

    type: Any
    """The Python data type of the value."""
    required: bool
    """This is a mandatory property."""

    mapper: Callable
    """The mapper function for Python to Bnn string conversion."""


class ValidationResultType(TypedDict):
    """The type definition for result of the validation method of Product3."""

    property: str
    """The name of the property where the error occurred."""

    error: str
    """The error type."""

    message: str
    """The error message."""


class ProductValidationResultType(TypedDict):
    """The type definition of the Product3 returned from the Bnn3 class."""

    property: str
    """The name of the property where the error occurred."""

    error: str
    """The error type."""

    message: str
    """The error message."""

    index: int
    """The index of the product in the products class."""


class BnnValidationType(TypedDict):
    """The type definition for result of validation method of Bnn3 class."""

    bnn: List[ValidationResultType]
    """A list if error for properties of the Bnn3 class."""

    products: List[ProductValidationResultType]
    """A list if error for properties of the Product3 class."""


ValidationValueType = Union[None, str, int, float]
"""The type definition for validatable values."""


BNN3_DEFINITION: List[DefinitionType] = [
        {
            'name': 'identifier',
            'max_length': 3,
            'options': None,
            'type': str,
            'required': True,
            'mapper': mapper.string_type
        },
        {
            'name': 'version',
            'max_length': 3,
            'options': None,
            'type': str,
            'required': True,
            'mapper': mapper.string_type
        },
        {
            'name': 'character_set',
            'max_length': 1,
            'options': (0, 1),
            'type': int,
            'required': True,
            'mapper': mapper.int_type
        },
        {
            'name': 'sender_address',
            'max_length': 50,
            'options': None,
            'type': str,
            'required': True,
            'mapper': mapper.string_type
        },
        {
            'name': 'scope',
            'max_length': 1,
            'options': ('V', 'T', 'S'),
            'type': str,
            'required': True,
            'mapper': mapper.string_type
        },
        {
            'name': 'content',
            'max_length': 30,
            'options': None,
            'type': str,
            'required': False,
            'mapper': mapper.string_type
        },
        {
            'name': 'currency',
            'max_length': 3,
            'options': None,
            'type': str,
            'required': True,
            'mapper': mapper.string_type
        },
        {
            'name': 'start_date',
            'max_length': 8,
            'options': None,
            'type': date,
            'required': False,
            'mapper': mapper.date_type
        },
        {
            'name': 'end_date',
            'max_length': 8,
            'options': None,
            'type': date,
            'required': False,
            'mapper': mapper.date_type
        },
        {
            'name': 'creation_date',
            'max_length': 12,
            'options': None,
            'type': datetime,
            'required': True,
            'mapper': mapper.datetime_type
        },
        {
            'name': 'file_counter',
            'max_length': 2,
            'options': None,
            'type': int,
            'required': True,
            'mapper': mapper.int_type
        },
        {
            'name': 'file_counter_next',
            'max_length': 2,
            'options': None,
            'type': int,
            'required': True,
            'mapper': mapper.int_type
        },
    ]


PRODUCT3_DEFINITION: List[DefinitionType] = [
    {
        'name': 'item_number',
        'max_length': 14,
        'options': None,
        'type': str,
        'required': True,
        'mapper': mapper.string_type
    },
    {
        'name': 'change_code',
        'max_length': 1,
        'options': ('N', 'A', 'X', 'R', 'V', 'W'),
        'type': str,
        'required': True,
        'mapper': mapper.string_type
    },
    {
        'name': 'change_date',
        'max_length': 12,
        'options': None,
        'type': datetime,
        'required': False,
        'mapper': mapper.datetime_type
    },
    {
        'name': 'ean_shop',
        'max_length': 14,
        'options': None,
        'type': str,
        'required': False,
        'mapper': mapper.string_type
    },
    {
        'name': 'ean_order',
        'max_length': 14,
        'options': None,
        'type': str,
        'required': False,
        'mapper': mapper.string_type
    },
    {
        'name': 'description',
        'max_length': 50,
        'options': None,
        'type': str,
        'required': True,
        'mapper': mapper.string_type
    },
    {
        'name': 'additional_description',
        'max_length': 50,
        'options': None,
        'type': str,
        'required': False,
        'mapper': mapper.string_type
    },
    {
        'name': 'label_description',
        'max_length': 30,
        'options': None,
        'type': str,
        'required': False,
        'mapper': mapper.string_type
    },
    {
        'name': 'commercial_category',
        'max_length': 5,
        'options': None,
        'type': str,
        'required': False,
        'mapper': mapper.string_type
    },
    {
        'name': 'brand_bnn_code',
        'max_length': 3,
        'options': None,
        'type': str,
        'required': True,
        'mapper': mapper.string_type
    },
    {
        'name': 'manufacturer_bnn_code',
        'max_length': 4,
        'options': None,
        'type': str,
        'required': False,
        'mapper': mapper.string_type
    },
    {
        'name': 'country_of_origin',
        'max_length': 3,
        'options': None,
        'type': str,
        'required': True,
        'mapper': mapper.string_type
    },
    {
        'name': 'quality_label_code',
        'max_length': 4,
        'options': None,
        'type': str,
        'required': True,
        'mapper': mapper.string_type
    },
    {
        'name': 'control_body',
        'max_length': 15,
        'options': None,
        'type': str,
        'required': False,
        'mapper': mapper.string_type
    },
    {
        'name': 'expiry_date_residual_time',
        'max_length': 4,
        'options': None,
        'type': int,
        'required': False,
        'mapper': mapper.int_type
    },
    {
        'name': 'product_group_bnn',
        'max_length': 4,
        'options': None,
        'type': int,
        'required': False,
        'mapper': mapper.int_type
    },
    {
        'name': 'product_group_ifh',
        'max_length': 2,
        'options': None,
        'type': int,
        'required': False,
        'mapper': mapper.int_type
    },
    {
        'name': 'product_group_vendor',
        'max_length': 4,
        'options': None,
        'type': int,
        'required': False,
        'mapper': mapper.int_type
    },
    {
        'name': 'replacement_item_number',
        'max_length': 14,
        'options': None,
        'type': str,
        'required': False,
        'mapper': mapper.string_type
    },
    {
        'name': 'minimum_order_quantity',
        'max_length': (5, 3),
        'options': None,
        'type': float,
        'required': False,
        'mapper': mapper.float_type
    },
    {
        'name': 'ordering_unit',
        'max_length': 15,
        'options': None,
        'type': str,
        'required': True,
        'mapper': mapper.string_type
    },
    {
        'name': 'ordering_unit_quantity',
        'max_length': (5, 3),
        'options': None,
        'type': float,
        'required': True,
        'mapper': mapper.float_type
    },
    {
        'name': 'shop_sales_unit',
        'max_length': 10,
        'options': None,
        'type': str,
        'required': True,
        'mapper': mapper.string_type
    },
    {
        'name': 'quantity_factor',
        'max_length': (5, 3),
        'options': None,
        'type': float,
        'required': True,
        'mapper': mapper.float_type
    },
    {
        'name': 'weighting_item',
        'max_length': 1,
        'options': ('J', 'N'),
        'type': str,
        'required': False,
        'mapper': mapper.string_type
    },
    {
        'name': 'deposit_number_shop_unit',
        'max_length': 10,
        'options': None,
        'type': str,
        'required': True,
        'mapper': mapper.string_type
    },
    {
        'name': 'deposit_number_ordering_unit',
        'max_length': 10,
        'options': None,
        'type': str,
        'required': False,
        'mapper': mapper.string_type
    },
    {
        'name': 'weight_shop_unit',
        'max_length': (5, 3),
        'options': None,
        'type': float,
        'required': False,
        'mapper': mapper.float_type
    },
    {
        'name': 'weight_ordering_unit',
        'max_length': (5, 3),
        'options': None,
        'type': float,
        'required': True,
        'mapper': mapper.float_type
    },
    {
        'name': 'width',
        'max_length': 3,
        'options': None,
        'type': int,
        'required': False,
        'mapper': mapper.int_type
    },
    {
        'name': 'height',
        'max_length': 3,
        'options': None,
        'type': int,
        'required': False,
        'mapper': mapper.int_type
    },
    {
        'name': 'depth',
        'max_length': 3,
        'options': None,
        'type': int,
        'required': False,
        'mapper': mapper.int_type
    },
    {
        'name': 'value_added_tax_code',
        'max_length': 1,
        'options': (1, 2, 3),
        'type': int,
        'required': True,
        'mapper': mapper.int_type
    },
    {
        'name': 'fixed_price_customer',
        'max_length': (5, 2),
        'options': None,
        'type': float,
        'required': False,
        'mapper': mapper.float_type
    },
    {
        'name': 'recommended_price_manufacturer',
        'max_length': (5, 2),
        'options': None,
        'type': float,
        'required': False,
        'mapper': mapper.float_type
    },
    {
        'name': 'recommended_price_vendor',
        'max_length': (5, 2),
        'options': None,
        'type': float,
        'required': False,
        'mapper': mapper.float_type
    },
    {
        'name': 'price',
        'max_length': (5, 2),
        'options': None,
        'type': float,
        'required': True,
        'mapper': mapper.float_type
    },
    {
        'name': 'discountable',
        'max_length': 1,
        'options': ('J', 'N'),
        'type': str,
        'required': False,
        'mapper': mapper.string_type
    },
    {
        'name': 'cash_discountable',
        'max_length': 1,
        'options': ('J', 'N'),
        'type': str,
        'required': False,
        'mapper': mapper.string_type
    },
    {
        'name': 'scale_quantity_1',
        'max_length': (5, 2),
        'options': None,
        'type': float,
        'required': False,
        'mapper': mapper.float_type
    },
    {
        'name': 'scale_quantity_1_price',
        'max_length': (5, 2),
        'options': None,
        'type': float,
        'required': False,
        'mapper': mapper.float_type
    },
    {
        'name': 'discountable_1',
        'max_length': 1,
        'options': ('J', 'N'),
        'type': str,
        'required': False,
        'mapper': mapper.string_type
    },
    {
        'name': 'cash_discountable_1',
        'max_length': 1,
        'options': ('J', 'N'),
        'type': str,
        'required': False,
        'mapper': mapper.string_type
    },
    {
        'name': 'scale_quantity_2',
        'max_length': (5, 2),
        'options': None,
        'type': float,
        'required': False,
        'mapper': mapper.float_type
    },
    {
        'name': 'scale_quantity_2_price',
        'max_length': (5, 2),
        'options': None,
        'type': float,
        'required': False,
        'mapper': mapper.float_type
    },
    {
        'name': 'discountable_2',
        'max_length': 1,
        'options': ('J', 'N'),
        'type': str,
        'required': False,
        'mapper': mapper.string_type
    },
    {
        'name': 'cash_discountable_2',
        'max_length': 1,
        'options': ('J', 'N'),
        'type': str,
        'required': False,
        'mapper': mapper.string_type
    },
    {
        'name': 'scale_quantity_3',
        'max_length': (5, 2),
        'options': None,
        'type': float,
        'required': False,
        'mapper': mapper.float_type
    },
    {
        'name': 'scale_quantity_3_price',
        'max_length': (5, 2),
        'options': None,
        'type': float,
        'required': False,
        'mapper': mapper.float_type
    },
    {
        'name': 'discountable_3',
        'max_length': 1,
        'options': ('J', 'N'),
        'type': str,
        'required': False,
        'mapper': mapper.string_type
    },
    {
        'name': 'cash_discountable_3',
        'max_length': 1,
        'options': ('J', 'N'),
        'type': str,
        'required': False,
        'mapper': mapper.string_type
    },
    {
        'name': 'scale_quantity_4',
        'max_length': (5, 2),
        'options': None,
        'type': float,
        'required': False,
        'mapper': mapper.float_type
    },
    {
        'name': 'scale_quantity_4_price',
        'max_length': (5, 2),
        'options': None,
        'type': float,
        'required': False,
        'mapper': mapper.float_type
    },
    {
        'name': 'discountable_4',
        'max_length': 1,
        'options': ('J', 'N'),
        'type': str,
        'required': False,
        'mapper': mapper.string_type
    },
    {
        'name': 'cash_discountable_4',
        'max_length': 1,
        'options': ('J', 'N'),
        'type': str,
        'required': False,
        'mapper': mapper.string_type
    },
    {
        'name': 'scale_quantity_5',
        'max_length': (5, 2),
        'options': None,
        'type': float,
        'required': False,
        'mapper': mapper.float_type
    },
    {
        'name': 'scale_quantity_5_price',
        'max_length': (5, 2),
        'options': None,
        'type': float,
        'required': False,
        'mapper': mapper.float_type
    },
    {
        'name': 'discountable_5',
        'max_length': 1,
        'options': ('J', 'N'),
        'type': str,
        'required': False,
        'mapper': mapper.string_type
    },
    {
        'name': 'cash_discountable_5',
        'max_length': 1,
        'options': ('J', 'N'),
        'type': str,
        'required': False,
        'mapper': mapper.string_type
    },
    {
        'name': 'item_type',
        'max_length': 1,
        'options': ('F', 'T', 'W', 'P', 'A'),
        'type': str,
        'required': False,
        'mapper': mapper.string_type
    },
    {
        'name': 'special_price',
        'max_length': 1,
        'options': 'A',
        'type': str,
        'required': False,
        'mapper': mapper.string_type
    },
    {
        'name': 'special_price_start_date',
        'max_length': 8,
        'options': None,
        'type': date,
        'required': False,
        'mapper': mapper.date_type
    },
    {
        'name': 'special_price_end_date',
        'max_length': 8,
        'options': None,
        'type': date,
        'required': False,
        'mapper': mapper.date_type
    },
    {
        'name': 'special_price_recommended_price',
        'max_length': (5, 2),
        'options': None,
        'type': float,
        'required': False,
        'mapper': mapper.float_type
    },
    {
        'name': 'base_price_unit',
        'max_length': 10,
        'options': None,
        'type': str,
        'required': True,
        'mapper': mapper.string_type
    },
    {
        'name': 'base_price_factor',
        'max_length': (5, 3),
        'options': None,
        'type': float,
        'required': True,
        'mapper': mapper.float_type
    },
    {
        'name': 'available_from',
        'max_length': 8,
        'options': None,
        'type': date,
        'required': False,
        'mapper': mapper.date_type
    },
    {
        'name': 'available_until',
        'max_length': 8,
        'options': None,
        'type': date,
        'required': False,
        'mapper': mapper.date_type
    },
]


@overload
def max_length_validation(value: str, length: int) -> None: ...


@overload
def max_length_validation(value: int, length: int) -> None: ...


@overload
def max_length_validation(value: float, length: Tuple[int, int]) -> None: ...


def max_length_validation(value: Any, length: Any) -> None:
    """
    Validate the maximum length of the string representation of the value.

    Args:
        value: The value to check.
        length: The maximum length of the value.

    Raises:
        bnn.exceptions.MaxLengthError: If the value is too long.
    """
    if isinstance(value, (int, float)):
        value_to_validate = str(value)

    elif isinstance(value, str):
        value_to_validate = value

    else:
        return None

    if isinstance(value, (int, str)) and len(value_to_validate) > length:
        raise bnn_ex.MaxLengthError(
                f'The length of the value is greater than "{length}".')

    if isinstance(value, float):
        before, after = value_to_validate.split('.')

        if len(before) > length[0] or len(after) > length[1]:
            raise bnn_ex.MaxLengthError(
                    f'The length of the value is greater than "{length}".')


def validate(
        value: ValidationValueType,
        definiton: DefinitionType,
        skip_type: bool = None,
        skip_options: bool = None,
        skip_max_length: bool = None,
        skip_required: bool = None
        ) -> None:
    """
    Validate the given value by the definiton.

    Args:
        value: The value to validate.
        definiton: The definition used.
        skip_type: Skip the type check.
        skip_options: Skip the options check.
        skip_max_length: Skip the maximum length check.
        skip_required: Skip the required check.

    Raises:
        TypeError: If the type of value is wrong.
        bnn.exceptions.OptionsError: If an option was not allowed.
        bnn.exceptions.MaxLengthError: If the value is too long.
        bnn.exceptions.RequiredError: If the required value does
            not exist or is None.

    """
    type_def = definiton['type']
    options_def = definiton['options']
    max_length_def = definiton['max_length']
    required_def = definiton['required']

    if value is not None:

        if not skip_type \
                and not isinstance(value, (date, datetime)) \
                and not isinstance(value, type_def):
            raise TypeError(
                    f'The Type of the value must be {type_def.__name__}.')

        if options_def \
                and not isinstance(value, (date, datetime)) \
                and not skip_options:
            if value not in options_def:
                raise bnn_ex.OptionsError(
                        f'The Value must be {str(options_def)}.')

        if max_length_def \
                and not isinstance(value, (date, datetime)) \
                and not skip_max_length:
            max_length_validation(
                    value, definiton['max_length'])  # type: ignore

    else:
        if required_def and not skip_required and not value:
            raise bnn_ex.RequiredError(
                    'The value is required.')
