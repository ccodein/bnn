"""
The module contains the Bnn3 class.

The classes map the data from the BNN file.
"""
from dataclasses import dataclass, field, fields
from datetime import date, datetime
from typing import Any, Optional, Union
from bnn.validation import validate, BNN3_DEFINITION, BnnValidationType
from bnn.products import Products
from bnn import exceptions as bnn_ex


@dataclass
class Bnn3():
    identifier: str = field(default='BNN', init=False)
    version: str = field(default='3', init=False)
    character_set: Optional[int] = 0
    sender_address: Optional[str] = None
    scope: Optional[str] = 'V'
    content: Optional[str] = None
    currency: Optional[str] = 'EUR'
    start_date: Optional[date] = None
    end_date: Optional[date] = None
    creation_date: Optional[datetime] = None
    file_counter: Optional[int] = 1
    file_counter_next: Optional[int] = 99

    def __post_init__(self) -> None:
        self.products: Products = Products()

    def __setattr__(self, name: str, value: Any) -> None:
        if name in ['identifier', 'version']:
            raise AttributeError(
                    f'The property {name} is read-only.')

        super().__setattr__(name, value)

    def validate(
            self,
            skip_type: bool = None,
            skip_options: bool = None,
            skip_max_length: bool = None,
            skip_required: bool = None
            ) -> Optional[BnnValidationType]:
        offset = 1
        result: BnnValidationType = {'bnn': [], 'products': []}
        column: Union[int, tuple]

        for i, f in enumerate(fields(self)[0:12]):
            try:
                message: str = ''
                error = None

                validate(
                    getattr(self, f.name),
                    BNN3_DEFINITION[i],
                    skip_type,
                    skip_options,
                    skip_max_length,
                    skip_required
                )

            except bnn_ex.MaxLengthError:
                error = 'MaxLengthError'
                message = 'The maximum length is ' \
                    f'"{BNN3_DEFINITION[i]["max_length"]}" characters.'

            except bnn_ex.OptionsError:
                error = 'OptionsError'
                message = 'Only the options ' \
                    f'"{BNN3_DEFINITION[i]["options"]}" are allowed.'

            except bnn_ex.RequiredError:
                error = 'RequiredError'
                message = 'The property is required.'

            except TypeError:
                error = 'TypeError'
                message = 'The property must be of ' \
                    f'"{BNN3_DEFINITION[i]["type"].__name__}" type.'

            if error:
                result['bnn'].append({
                    'property': f.name,
                    'error': error,
                    'message': message,
                })

            if f.name == 'creation_date':
                offset += 1

        for i, product in enumerate(self.products):
            product_result: Any = product.validate(
                skip_type,
                skip_options,
                skip_max_length,
                skip_required
            )

            if product_result:
                for x, _ in enumerate(product_result):
                    product_result[x]['index'] = i+2

                result['products'].extend(product_result)

        if result == {'bnn': [], 'products': []}:
            return None

        return result
