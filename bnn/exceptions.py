"""The exceptions module."""


class FileFormatError(Exception):
    """Raised if a file is not in the correct format."""


class MaxLengthError(Exception):
    """Raised if the maximum length has been exceeded."""


class OptionsError(Exception):
    """Raised if not an option."""


class RequiredError(Exception):
    """Raised if something required does not exist.."""
