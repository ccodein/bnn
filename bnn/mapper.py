"""
The mapper module.

This provides mapper functions for the type conversion
between Python types and the string representation
in the BNN file.
"""
from datetime import date, datetime
from typing import Literal, Tuple, Optional, overload, Any


@overload
def int_type(value: str, inverse: Literal[True]) -> Optional[int]:
    ...


@overload
def int_type(value: int, inverse: Literal[False]) -> str:
    ...


def int_type(value: Any, inverse: Any = False) -> Any:
    """
    Convert Python int type.

    This type mapper function converts a Python int type into
    a string or vice-versa with use of inverse parameter.
    The function returns None if the string is empty and an empty
    string if the value is None.

    Args:
        value: The value to convert.
        inverse: If False, the passed integer or None is converted
            to a string representation. If inverse is True, the
            passed string is converted to an integer or None.

    Raises:
        TypeError: If the value is not in the right format and
            therefore cannot be converted.
    """
    if not inverse:
        if value is None:
            return ''

        if isinstance(value, int):
            return str(value)

        raise TypeError('Value must be an int.')

    if inverse:
        if isinstance(value, str):
            if not value or value.isspace():
                return None
            try:
                return int(value)
            except Exception:
                pass

        raise TypeError('Value must be a string in format "45".')


@overload
def float_type(value: str, inverse: Literal[True]) -> Optional[float]:
    ...


@overload
def float_type(value: Optional[float], inverse: Literal[False]) -> str:
    ...


def float_type(value: Any, inverse: Any = False) -> Any:
    """
    Convert Python float type.

    This type mapper function converts a Python float type into
    a string or vice-versa with use of inverse parameter.
    The function returns None if the string is empty and an empty
    string if the value is None.

    Args:
        value: The value to convert.
        inverse: If False, the passed float or None is converted to a
            string representation. If inverse is True, the passed string
            is converted to an float or None.

    Raises:
        TypeError: If the value is not in the right format and
            therefore cannot be converted.
    """
    if not inverse:
        if value is None:
            return ''

        if isinstance(value, float):
            if value.is_integer():
                return str(int(value))

            return str(value)

        raise TypeError('Value must be an float')

    if inverse:
        if isinstance(value, str):
            if not value or value.isspace():
                return None

            try:
                return float(value.replace(',', '.'))
            except Exception:
                pass

        raise TypeError('Value must be a string in format "45" or "45,3".')


@overload
def string_type(value: str, inverse: Literal[True]) -> Optional[str]:
    ...


@overload
def string_type(value: Optional[str], inverse: Literal[False]) -> str:
    ...


def string_type(value: Any, inverse: Any = False) -> Any:
    """
    Convert Python string type.

    This type mapper function converts a Python string type into
    a string or vice-versa with use of inverse parameter.
    The function returns None if the string is empty and an empty
    string if the value is None.

    Args:
        value: The value to convert.
        inverse: If False, the passed string or None is converted to a string
            representation. If inverse is True, the passed string is
            converted to an string or None.

    Raises:
        TypeError: If the value is not in the right format and
            therefore cannot be converted.
    """
    if not inverse:
        if isinstance(value, str) or value is None:
            if not value or value.isspace():
                return ''

            return value

        raise TypeError('Value must be a string.')

    if inverse:
        if isinstance(value, str):
            if not value:
                return None

            return value

        raise TypeError('Value must be a string.')


@overload
def date_type(value: str, inverse: Literal[True]) -> Optional[date]:
    ...


@overload
def date_type(value: Optional[date], inverse: Literal[False]) -> str:
    ...


def date_type(value: Any, inverse: Any = False) -> Any:
    """
    Convert a Python date type.

    This type mapper function converts a Python date type into a string of
    the format YYYYMMDD  or vice-versa with use of inverse parameter.
    The function returns None if the string is empty and an empty
    string if the value is None.

    Args:
        value: The value to convert.
        inverse: If False, the passed date or None is converted to a string
            representation. If inverse is True, the passed string is
            converted to an date or None.

    Raises:
        TypeError: If the value is not in the right format and
            therefore cannot be converted.
    """
    if not inverse:
        if value is None:
            return ''

        if isinstance(value, date):
            date_str = value.strftime('%Y%m%d')

            return date_str

        raise TypeError('The date must be a datetime.date')

    if inverse:
        if isinstance(value, str):
            if value in ('', '0', '00000000') or value.isspace():
                return None

            try:
                return date(
                    int(value[0:4]),
                    int(value[4:6]),
                    int(value[6:8])
                )
            except Exception:
                pass

        raise TypeError('The date must be a string in format "YYYYMMDD"')


@overload
def datetime_type(value: Tuple[str], inverse: Literal[True]) -> Optional[date]:
    ...


@overload
def datetime_type(value: Optional[datetime], inverse: Literal[False]) -> str:
    ...


def datetime_type(value: Any, inverse: Any = False) -> Any:
    """
    Convert a Python datetime type.

    This type mapper function converts a Python datetime type into a list
    of strings or vice-versa with use of inverse parameter. The first
    element in the value tuple is the date in the format YYYYMMDD and the
    second one is the time in the format HHMM. The function returns None if
    the string is empty and an empty string if the value is None.

    Args:
        value: The value to convert.
        inverse: If False, the passed datetime or None is converted to a string
            representation. If inverse is True, the passed string is
            converted to an datetime or None.

    Raises:
        TypeError: If the value is not in the right format and
            therefore cannot be converted.
    """
    if not inverse:
        if value is None:
            return ''

        if isinstance(value, datetime):
            date_str = value.strftime('%Y%m%d')
            time_str = value.strftime('%H%M')

            return [date_str, time_str]

        raise TypeError('The date must be a datetime.date')

    if inverse:
        if isinstance(value, list):
            if (value[0] in ('', '0', '00000000') or value[0].isspace()) or \
                    (value[1] in ('', '0', '0000') or value[1].isspace()):
                return None

            try:
                return datetime(
                    int(value[0][0:4]),
                    int(value[0][4:6]),
                    int(value[0][6:8]),
                    int(value[1][0:2]),
                    int(value[1][2:4])
                )
            except Exception:
                pass

        raise TypeError(
            'The date must be a list in format ["YYYYMMDD", "HHMM"]')
