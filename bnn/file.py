"""The file module contains classes and functions for BNN file handling."""
import csv
from dataclasses import fields, astuple
from bnn.bnn import Bnn3, BNN3_DEFINITION
from bnn.products import Product3, PRODUCT3_DEFINITION
from bnn import exceptions as bnn_ex
from typing import Optional, Any, List, TypedDict, Union, Tuple
from types import TracebackType
from datetime import date, datetime


class ConversionErrorType(TypedDict):
    """The type definition for value conversion errors."""

    value: Union[str, int, float, date, datetime, List[str]]
    """The values at which the error occurred."""

    message: str
    """The error message"""

    row: Union[int, str]
    """The row in the BNN file."""

    column: Union[int, tuple]
    """The column in the BNN file."""


def _header_to_bnn(
        header: List[str], bnn: Bnn3) -> Optional[List[ConversionErrorType]]:
    """
    Add values from the header row of BNN file to the Bnn3 class.

    Args:
        header: The header list from the BNN file.
        bnn: A Bnn3 class instance.

    Returns:
        List of Errors of None if no error occurred.
    """
    offset = 0
    errors: List[ConversionErrorType] = []

    for i, field in enumerate(fields(bnn)[2:-1], start=2):
        mapper: Any = BNN3_DEFINITION[i]['mapper']
        column: Union[int, tuple] = i + offset + 1
        value: Union[str, List[str]]

        try:
            if field.name == 'creation_date':
                value = header[i:i+2]
                column = (i+1, i+2)
                offset += 1
                bnn.creation_date = mapper(value, inverse=True)

                continue

            value = header[i+offset]
            setattr(bnn, field.name, mapper(value, inverse=True))

        except TypeError as ex:
            errors.append(
                {
                    'value': value,
                    'message': str(ex),
                    'row': 'first',
                    'column': column
                }
            )

            setattr(bnn, field.name, None)

    if not errors:
        return None

    return errors


def _products_to_bnn(
        products: List[List[str]],
        bnn: Bnn3
        ) -> Optional[List[ConversionErrorType]]:
    """
    Add values from the product rows of BNN file to the Bnn3 class.

    Args:
        products: The products list from the BNN file.
        bnn: A Bnn3 class instance.

    Returns:
        List of Errors of None if no error occurred.
    """
    errors: List[ConversionErrorType] = []

    for n, product_row in enumerate(products):
        product = Product3()
        offset = 0

        for i, field in enumerate(fields(product)):
            # some suppliers end the product line with ';'
            # what means new column in CSV
            if i == 68:
                break

            mapper = PRODUCT3_DEFINITION[i]['mapper']
            column = i + offset + 1

            try:
                if field.name == 'change_date':
                    offset += 1

                    product.change_date = mapper(
                        product_row[i:i+2], inverse=True)

                    continue

                value = product_row[i+offset]

                setattr(
                    product, field.name, mapper(
                        product_row[i+offset], inverse=True)
                )

            except TypeError as ex:
                errors.append(
                    {
                        'value': value,
                        'message': str(ex),
                        'row': n+2,
                        'column': column
                    }
                )

                setattr(bnn, field.name, None)

        bnn.products.add(product)

    if not errors:
        return None

    return errors


def _footer_to_bnn(
        footer: List[str], bnn: Bnn3) -> Optional[List[ConversionErrorType]]:
    """
    Add values from the footer row of BNN file to the Bnn3 class.

    Args:
        header: The footer list from the BNN file.
        bnn: A Bnn3 class instance.

    Returns:
        List of Errors of None if no error occurred.
    """
    mapper: Any = BNN3_DEFINITION[-1]['mapper']
    errors: List[ConversionErrorType] = []

    try:
        value = footer[2]
        bnn.file_counter_next = mapper(value, inverse=True)

    except TypeError as ex:
        errors.append(
            {
                'value': value,
                'message': str(ex),
                'row': 'last',
                'column': 3
            }
        )

        bnn.file_counter_next = None

    if not errors:
        return None

    return errors


class BnnFile():
    """
    A context manager for BNN file operations.

    This Context Manager can be used to create or read a BNN file.

    Example for reading a BNN file:

    .. code:: python

        from bnn.file import BnnFile

        with BnnFile(<path_to_file>, 'r') as bnn_file:
            bnn, errors = bnn_file.read()

    Example for writing a BNN instance:

    .. code:: python

        from bnn.file import BnnFile
        from bnn.bnn import Bnn3

        bnn = Bnn3()

        with BnnFile(<path_to_file>, 'w') as bnn_file:
            bnn_file.write(bnn)
    """

    def __init__(self, filename: str, mode: str) -> None:
        """
        Initialize a BnnFile instance.

        Args:
            filename: The filename including the path.
            mode: The mode to open (r: for reading, w: for writing)
        """
        self.filename = filename
        self.mode = mode

    def __enter__(self) -> 'BnnFile':
        """Call when entering the context."""
        self._file = open(self.filename, self.mode,  encoding='ISO-8859-1')

        return self

    def __exit__(
            self,
            exc_type: Optional[type],
            exc_value: Optional[Exception],
            exc_traceback: TracebackType
            ) -> bool:
        """Call when exit the context."""
        self._file.close()

        if exc_type:
            return False

        return True

    def read(self) -> Tuple[Bnn3, Optional[List[ConversionErrorType]]]:
        """
        Read a BNN file and return a Bnn instance and a conversion error list.

        The error list looks like this and contains all occurred errors. If
        no error occurred, None is returned istead a list.

        .. code:: python

            [
                {
                    'value': 'X',
                    'message': 'Value must be a string in format "45".',
                    'row': 'first',
                    'column': 3
                },
                {
                    'value': 'a',
                    'message': 'The date must a string in format "YYYYMMDD"',
                    'row': 'first',
                    'column': 8
                },
            ]

        Returns:
            The method returns a tuple. The first item is Bnn class instance,
                the second is conversion error list.

        """
        try:
            reader = csv.reader(self._file, delimiter=';')
            rows = [row for row in reader]

            header = rows[0]
            products = rows[1:-1]
            footer = rows[-1]

            if len(header) != 12 or len(footer) != 3 or header[0] != 'BNN':
                raise Exception()

        except Exception:
            raise bnn_ex.FileFormatError(
                'The BNN file is not in the correct format.'
            )

        bnn = Bnn3()
        errors = []
        header_errors = _header_to_bnn(header, bnn)
        product_errors = _products_to_bnn(products, bnn)
        footer_errors = _footer_to_bnn(footer, bnn)

        if header_errors:
            errors.extend(header_errors)

        if product_errors:
            errors.extend(product_errors)

        if footer_errors:
            errors.extend(footer_errors)

        if errors:
            return bnn, errors

        return bnn, None

    def write(self, bnn: Bnn3) -> None:
        """
        Write the Bnn instance to a file.

        Args:
            bnn: A Bnn class instance to be written.
        """
        data = []
        row = []
        bnn_data = astuple(bnn)

        for i, value in enumerate(bnn_data[:-1]):
            mapper = BNN3_DEFINITION[i]['mapper']

            if i == 9:
                date = mapper(value)
                row.append(date[0])
                row.append(date[1])

                continue

            row.append(mapper(value))

        data.append(row)

        for product in bnn.products:
            row = []

            for i, value in enumerate(astuple(product)):
                mapper = PRODUCT3_DEFINITION[i]['mapper']

                if i == 2:
                    date = mapper(value)
                    row.append(date[0])
                    row.append(date[1])

                    continue

                row.append(mapper(value))

            data.append(row)

        data.append([None, None, bnn_data[-1]])

        writer = csv.writer(self._file, delimiter=';')
        writer.writerows(data)
