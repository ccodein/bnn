from dataclasses import dataclass, fields
from datetime import date, datetime
from typing import Optional, Union, List, Iterator
from bnn.validation import (
    validate, PRODUCT3_DEFINITION, ValidationResultType
)
from bnn import exceptions as bnn_ex


@dataclass
class Product3():
    item_number: Optional[str] = None
    change_code: Optional[str] = None
    change_date: Optional[datetime] = None
    ean_shop: Optional[str] = None
    ean_order: Optional[str] = None
    description: Optional[str] = None
    additional_description: Optional[str] = None
    label_description: Optional[str] = None
    commercial_category: Optional[str] = None
    brand_bnn_code: Optional[str] = None
    manufacturer_bnn_code: Optional[str] = None
    country_of_origin: Optional[str] = None
    quality_label_code: Optional[str] = None
    control_body: Optional[str] = None
    expiry_date_residual_time: Optional[str] = None
    product_group_bnn: Optional[str] = None
    product_group_ifh: Optional[str] = None
    product_group_vendor: Optional[str] = None
    replacement_item_number: Optional[str] = None
    minimum_order_quantity: Optional[float] = None
    ordering_unit: Optional[str] = None
    ordering_unit_quantity: Optional[float] = None
    shop_sales_unit: Optional[str] = None
    quantity_factor: Optional[float] = None
    weighting_item: Optional[str] = None
    deposit_number_shop_unit: Optional[str] = None
    deposit_number_ordering_unit: Optional[str] = None
    weight_shop_unit: Optional[float] = None
    weight_ordering_unit: Optional[float] = None
    width: Optional[int] = None
    height: Optional[int] = None
    depth: Optional[int] = None
    value_added_tax_code: Optional[int] = None
    fixed_price_customer: Optional[float] = None
    recommended_price_manufacturer: Optional[float] = None
    recommended_price_vendor: Optional[float] = None
    price: Optional[float] = None
    discountable: Optional[str] = None
    cash_discountable: Optional[str] = None
    scale_quantity_1: Optional[float] = None
    scale_quantity_1_price: Optional[float] = None
    discountable_1: Optional[str] = None
    cash_discountable_1: Optional[str] = None
    scale_quantity_2: Optional[float] = None
    scale_quantity_2_price: Optional[float] = None
    discountable_2: Optional[str] = None
    cash_discountable_2: Optional[str] = None
    scale_quantity_3: Optional[float] = None
    scale_quantity_3_price: Optional[float] = None
    discountable_3: Optional[str] = None
    cash_discountable_3: Optional[str] = None
    scale_quantity_4: Optional[float] = None
    scale_quantity_4_price: Optional[float] = None
    discountable_4: Optional[str] = None
    cash_discountable_4: Optional[str] = None
    scale_quantity_5: Optional[float] = None
    scale_quantity_5_price: Optional[float] = None
    discountable_5: Optional[str] = None
    cash_discountable_5: Optional[str] = None
    item_type: Optional[str] = None
    special_price: Optional[str] = None
    special_price_start_date: Optional[date] = None
    special_price_end_date: Optional[date] = None
    special_price_recommended_price: Optional[float] = None
    base_price_unit: Optional[str] = None
    base_price_factor: Optional[float] = None
    available_from: Optional[date] = None
    available_until: Optional[date] = None

    def validate(
            self,
            skip_type: bool = None,
            skip_options: bool = None,
            skip_max_length: bool = None,
            skip_required: bool = None
            ) -> Optional[List[ValidationResultType]]:
        offset = 1
        result: List[ValidationResultType] = []
        column: Union[int, tuple]

        for i, f in enumerate(fields(self)):
            try:
                message = ''
                error: Optional[str] = None

                validate(
                    getattr(self, f.name),
                    PRODUCT3_DEFINITION[i],
                    skip_type,
                    skip_options,
                    skip_max_length,
                    skip_required
                )

            except bnn_ex.MaxLengthError:
                error = 'MaxLengthError'
                message = 'The maximum length is ' \
                    f'"{PRODUCT3_DEFINITION[i]["max_length"]}" characters.'

            except bnn_ex.OptionsError:
                error = 'OptionsError'
                message = 'Only the options ' \
                    f'"{PRODUCT3_DEFINITION[i]["options"]}" are allowed.'

            except bnn_ex.RequiredError:
                error = 'RequiredError'
                message = 'The property is required.'

            except TypeError:
                error = 'TypeError'
                message = 'The property must be of ' \
                    f'"{PRODUCT3_DEFINITION[i]["type"].__name__}" type.'

            if error:
                result.append({
                    'property': f.name,
                    'error': error,
                    'message': message,
                })

            if f.name == 'change_date':
                offset += 1

        if result:
            return result

        return None


class Products:
    def __init__(self) -> None:
        self._data: List[Product3] = []

    def __getitem__(self, index: int) -> Product3:
        return self._data[index]

    def __len__(self) -> int:
        return len(self._data)

    def __iter__(self) -> Iterator[Product3]:
        self._i = 0

        return self

    def __next__(self) -> Product3:
        if self._i < len(self._data):
            self._i += 1

            return self._data[self._i-1]
        else:
            raise StopIteration()

    def add(self, product: Product3) -> None:
        self._data.append(product)

    def pop(self, index: int) -> Product3:
        return self._data.pop(index)

    def remove(self, value: Product3) -> None:
        self._data.remove(value)
