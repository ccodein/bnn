:gitlab_url: https://gitlab.com/ccodein/bnn

The *bnn* Python package
========================
This Python package implements the software interface for the exchange of
price and product information of the *Bundesverband Naturkost
Naturwaren (BNN) e.V.*
The interface is widely used in the German organic food trade and is
used by suppliers and stores.

With the bnn package files can be read, created and validated.

.. Note::
   The current version of the package implements version 3 of the
   BNN standard. If a higher version is needed, please contact us
   via the `project's issues tracker <https://gitlab.com/ccodein/bnn/-/issues>`_.


.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Contents

   API <api/bnn>

.. toctree::
   :caption: Links
   :maxdepth: 1

   Gitlab Project <http://gitlab.com/ccodein/bnn/>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
