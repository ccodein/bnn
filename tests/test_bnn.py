"""Tests for the bnn module."""
import pytest
from datetime import date, datetime
from bnn.bnn import Bnn3


class TestBnn3Identifier():
    """Tests for the identifier property."""

    def test_check_default(self, bnn):
        """Check the default property."""
        assert bnn.identifier == 'BNN'

    def test_is_read_only(self, bnn):
        """Is a read only property."""
        with pytest.raises(AttributeError):
            bnn.identifier = 'xyz'


class TestBnn3Version():
    """Tests for the version property."""

    def test_check_default(self, bnn):
        """Check the default property."""
        assert bnn.version == '3'

    def test_is_read_only(self, bnn):
        """Is a read only property."""
        with pytest.raises(AttributeError):
            bnn.version = '4.1'


class TestBnn3CharacterSet():
    """Tests for the character_set property."""

    def test_check_default(self, bnn):
        """Check the default property."""
        assert bnn.character_set == 0

    @pytest.mark.parametrize('option', (0, 1))
    def test_set_property(self, option, bnn):
        """Set the property."""
        bnn.character_set = option
        bnn.validate()

    def test_wrong_option(self, bnn):
        """Is a worng option."""
        bnn.character_set = 2
        result = bnn.validate()

        assert result == {
            'bnn': [
                {
                    'property': 'character_set',
                    'error': 'OptionsError',
                    'message': 'Only the options "(0, 1)" are allowed.',
                }
            ],
            'products': []
        }

    def test_wrong_type(self, bnn):
        """Is a worng type."""
        bnn.character_set = 'x'
        result = bnn.validate()

        assert result == {
            'bnn': [
                {
                    'property': 'character_set',
                    'error': 'TypeError',
                    'message': 'The property must be of "int" type.',
                }
            ],
            'products': []
        }


class TestBnn3SenderAddress():
    """Tests for the sender_address property."""

    def test_check_default(self):
        """Check the default property."""
        bnn = Bnn3()

        assert not bnn.sender_address

    def test_set_property(self, bnn):
        """Set the property."""
        bnn.sender_address == 'x'*50
        bnn.validate()

    def test_max_length_error(self, bnn):
        """String is too long."""
        bnn.sender_address = 'x'*51
        result = bnn.validate()

        assert result == {
            'bnn': [
                {
                    'property': 'sender_address',
                    'error': 'MaxLengthError',
                    'message': 'The maximum length is "50" characters.',
                }
            ],
            'products': []
        }

    def test_wrong_type(self, bnn):
        """Is a worng type."""
        bnn.sender_address = 123
        result = bnn.validate()

        assert result == {
            'bnn': [
                {
                    'property': 'sender_address',
                    'error': 'TypeError',
                    'message': 'The property must be of "str" type.',
                }
            ],
            'products': []
        }


class TestBnn3Scope():
    """Tests for the scope property."""

    def test_check_default(self):
        """Check the default property."""
        bnn = Bnn3()

        assert bnn.scope == 'V'

    @pytest.mark.parametrize('option', ('V', 'T', 'S'))
    def test_set_property(self, option, bnn):
        """Set the property."""
        bnn.scope = option
        bnn.validate()

    def test_wrong_option(self, bnn):
        """Is a worng option."""
        bnn.scope = 'G'
        result = bnn.validate()

        assert result == {
            'bnn': [
                {
                    'property': 'scope',
                    'error': 'OptionsError',
                    'message': 'Only the options "(\'V\', \'T\', \'S\')" '
                    'are allowed.',
                }
            ],
            'products': []
        }


class TestBnn3Content():
    """Tests for the content property."""

    def test_check_default(self):
        """Check the default property."""
        bnn = Bnn3()

        assert not bnn.content

    def test_set_property(self, bnn):
        """Set the property."""
        bnn.content = 'some text'
        bnn.validate()

    def test_max_length_error(self, bnn):
        """String is too long."""
        bnn.content = 'x'*31
        result = bnn.validate()

        assert result == {
            'bnn': [
                {
                    'property': 'content',
                    'error': 'MaxLengthError',
                    'message': 'The maximum length is "30" characters.',
                }
            ],
            'products': []
        }

    def test_wrong_type(self, bnn):
        """Is a worng type."""
        bnn.content = 123
        result = bnn.validate()

        assert result == {
            'bnn': [
                {
                    'property': 'content',
                    'error': 'TypeError',
                    'message': 'The property must be of "str" type.',
                }
            ],
            'products': []
        }


class TestBnn3Currency():
    """Tests for the currency property."""

    def test_check_default(self):
        """Check the default property."""
        bnn = Bnn3()

        assert bnn.currency == 'EUR'

    def test_set_property(self, bnn):
        """Set the property."""
        bnn.currency = 'x'*3
        bnn.validate()

    def test_max_length_error(self, bnn):
        """String is too long."""
        bnn.currency = 'x'*4
        result = bnn.validate()

        assert result == {
            'bnn': [
                {
                    'property': 'currency',
                    'error': 'MaxLengthError',
                    'message': 'The maximum length is "3" characters.',
                }
            ],
            'products': []
            }

    def test_wrong_type(self, bnn):
        """Is a worng type."""
        bnn.currency = 123
        result = bnn.validate()

        assert result == {
            'bnn': [
                {
                    'property': 'currency',
                    'error': 'TypeError',
                    'message': 'The property must be of "str" type.',
                }
            ],
            'products': []
        }


class TestBnn3StartDate():
    """Tests for the start_date property."""

    def test_check_default(self):
        """Check the default property."""
        bnn = Bnn3()

        assert not bnn.start_date

    def test_set_property(self, bnn):
        """Set the property."""
        bnn.start_date = date(2021, 3, 6)
        bnn.validate()

    def test_wrong_type(self, bnn):
        """Is a worng type."""
        bnn.start_date = 123
        result = bnn.validate()

        assert result == {
            'bnn': [
                {
                    'property': 'start_date',
                    'error': 'TypeError',
                    'message': 'The property must be of "date" type.',
                }
            ],
            'products': []
        }


class TestBnn3EndDate():
    """Tests for the end_date property."""

    def test_check_default(self):
        """Check the default property."""
        bnn = Bnn3()

        assert not bnn.end_date

    def test_set_property(self, bnn):
        """Set the property."""
        bnn.end_date = date(2021, 3, 6)
        bnn.validate()

    def test_wrong_type(self, bnn):
        """Is a worng type."""
        bnn.end_date = 123
        result = bnn.validate()

        assert result == {
            'bnn': [
                {
                    'property': 'end_date',
                    'error': 'TypeError',
                    'message': 'The property must be of "date" type.',
                }
            ],
            'products': []
        }


class TestBnn3CreationDate():
    """Tests for the end_date property."""

    def test_check_default(self):
        """Check the default property."""
        bnn = Bnn3()

        assert not bnn.end_date

    def test_set_property(self, bnn):
        """Set the property."""
        bnn.creation_date = datetime(2021, 3, 6)
        bnn.validate()

    def test_wrong_type(self, bnn):
        """Is a worng type."""
        bnn.creation_date = 123
        result = bnn.validate()

        assert result == {
            'bnn': [
                {
                    'property': 'creation_date',
                    'error': 'TypeError',
                    'message': 'The property must be of "datetime" type.',
                }
            ],
            'products': []
        }


class TestBnn3FileCounter():
    """Tests for the file_counter property."""

    def test_check_default(self):
        """Check the default property."""
        bnn = Bnn3()

        assert bnn.file_counter == 1

    def test_set_property(self, bnn):
        """Set the property."""
        bnn.file_counter = 2
        bnn.validate()

    def test_max_length_error(self, bnn):
        """String is too long."""
        bnn.file_counter = 123
        result = bnn.validate()

        assert result == {
            'bnn': [
                {
                    'property': 'file_counter',
                    'error': 'MaxLengthError',
                    'message': 'The maximum length is "2" characters.',
                }
            ],
            'products': []
        }

    def test_wrong_type(self, bnn):
        """Is a worng type."""
        bnn.file_counter = 'x'
        result = bnn.validate()

        assert result == {
            'bnn': [
                {
                    'property': 'file_counter',
                    'error': 'TypeError',
                    'message': 'The property must be of "int" type.',
                }
            ],
            'products': []
        }


class TestBnn3FileCounterNext():
    """Tests for the file_counter_next property."""

    def test_check_default(self):
        """Check the default property."""
        bnn = Bnn3()

        assert bnn.file_counter_next == 99

    def test_set_property(self, bnn):
        """Set the property."""
        bnn.file_counter_next = 2
        bnn.validate()

    def test_max_length_error(self, bnn):
        """String is too long."""
        bnn.file_counter_next = 123
        result = bnn.validate()

        assert result == {
            'bnn': [
                {
                    'property': 'file_counter_next',
                    'error': 'MaxLengthError',
                    'message': 'The maximum length is "2" characters.',
                }
            ],
            'products': []
        }

    def test_wrong_type(self, bnn):
        """Is a worng type."""
        bnn.file_counter_next = 'x'
        result = bnn.validate()

        assert result == {
            'bnn': [
                {
                    'property': 'file_counter_next',
                    'error': 'TypeError',
                    'message': 'The property must be of "int" type.',
                }
            ],
            'products': []
        }


class TestBnn3Validate:
    """Tests for the validate method."""

    def test_all_errors_found(self, bnn, product):
        """All errors were found."""
        product.item_number = None
        bnn.content = 31*'x'
        bnn.character_set = 3
        bnn.currency = 3
        bnn.file_counter_next = None
        bnn.products.add(product)

        result = bnn.validate()

        assert result == {
            'bnn': [
                {
                    'property': 'character_set',
                    'error': 'OptionsError',
                    'message': 'Only the options "(0, 1)" are allowed.',
                },
                {
                    'property': 'content',
                    'error': 'MaxLengthError',
                    'message': 'The maximum length is "30" characters.',
                },
                {
                    'property': 'currency',
                    'error': 'TypeError',
                    'message': 'The property must be of "str" type.',
                },
                {
                    'property': 'file_counter_next',
                    'error': 'RequiredError',
                    'message': 'The property is required.',
                }
            ],
            'products': [
                {
                    'property': 'item_number',
                    'error': 'RequiredError',
                    'message': 'The property is required.',
                    'index': 2
                }
            ]
        }

    def test_skip_type(self, bnn, product):
        """Skip type validation."""
        product.item_number = None
        bnn.content = 31*'x'
        bnn.character_set = 3
        bnn.currency = 3
        bnn.file_counter_next = None
        bnn.products.add(product)

        result = bnn.validate(skip_type=True)

        assert result == {
            'bnn': [
                {
                    'property': 'character_set',
                    'error': 'OptionsError',
                    'message': 'Only the options "(0, 1)" are allowed.',
                },
                {
                    'property': 'content',
                    'error': 'MaxLengthError',
                    'message': 'The maximum length is "30" characters.',
                },
                {
                    'property': 'file_counter_next',
                    'error': 'RequiredError',
                    'message': 'The property is required.',
                }
            ],
            'products': [
                {
                    'property': 'item_number',
                    'error': 'RequiredError',
                    'message': 'The property is required.',
                    'index': 2
                }
            ]
        }

    def test_skip_options(self, bnn, product):
        """Skip options validation."""
        product.item_number = None
        bnn.content = 31*'x'
        bnn.character_set = 3
        bnn.currency = 3
        bnn.file_counter_next = None
        bnn.products.add(product)

        result = bnn.validate(skip_options=True)

        assert result == {
            'bnn': [
                {
                    'property': 'content',
                    'error': 'MaxLengthError',
                    'message': 'The maximum length is "30" characters.',
                },
                {
                    'property': 'currency',
                    'error': 'TypeError',
                    'message': 'The property must be of "str" type.',
                },
                {
                    'property': 'file_counter_next',
                    'error': 'RequiredError',
                    'message': 'The property is required.',
                }
            ],
            'products': [
                {
                    'property': 'item_number',
                    'error': 'RequiredError',
                    'message': 'The property is required.',
                    'index': 2
                }
            ]
        }

    def test_skip_max_length(self, bnn, product):
        """Skip the max_length validation."""
        product.item_number = None
        bnn.content = 31*'x'
        bnn.character_set = 3
        bnn.currency = 3
        bnn.file_counter_next = None
        bnn.products.add(product)

        result = bnn.validate(skip_max_length=True)

        assert result == {
            'bnn': [
                {
                    'property': 'character_set',
                    'error': 'OptionsError',
                    'message': 'Only the options "(0, 1)" are allowed.',
                },
                {
                    'property': 'currency',
                    'error': 'TypeError',
                    'message': 'The property must be of "str" type.',
                },
                {
                    'property': 'file_counter_next',
                    'error': 'RequiredError',
                    'message': 'The property is required.',
                }
            ],
            'products': [
                {
                    'property': 'item_number',
                    'error': 'RequiredError',
                    'message': 'The property is required.',
                    'index': 2
                }
            ]
        }

    def test_skip_required(self, bnn, product):
        """Skip required value validation."""
        product.item_number = None
        bnn.content = 31*'x'
        bnn.character_set = 3
        bnn.currency = 3
        bnn.file_counter_next = None
        bnn.products.add(product)

        result = bnn.validate(skip_required=True)

        assert result == {
            'bnn': [
                {
                    'property': 'character_set',
                    'error': 'OptionsError',
                    'message': 'Only the options "(0, 1)" are allowed.',
                },
                {
                    'property': 'content',
                    'error': 'MaxLengthError',
                    'message': 'The maximum length is "30" characters.',
                },
                {
                    'property': 'currency',
                    'error': 'TypeError',
                    'message': 'The property must be of "str" type.',
                }
            ],
            'products': []
        }

    def test_no_validation_error_occurred(self, bnn, product):
        """No validation error occurred."""
        bnn.products.add(product)

        result = bnn.validate()

        assert not result
