"""Tests for the file module."""
import pytest
from bnn.file import (
    BnnFile, _header_to_bnn, _footer_to_bnn, _products_to_bnn
)
from bnn import exceptions as bnn_ex
from bnn.bnn import Bnn3
import datetime


class TestHeaderToBnn:
    """Tests for the _header_to_bnn function."""

    def test_no_error(self):
        """No error occurred."""
        bnn = Bnn3()
        header = [
            'BNN',
            '3',
            '0',
            "Terra Naturkost Handels KG",
            "T",
            "Naturdrogerie 12.15",
            "EUR",
            '20151130',
            '20160131',
            '20151123',
            '1644',
            '1'
        ]
        assert not _header_to_bnn(header, bnn)
        assert bnn.identifier == 'BNN'
        assert bnn.version == '3'
        assert bnn.character_set == 0
        assert bnn.sender_address == 'Terra Naturkost Handels KG'
        assert bnn.scope == 'T'
        assert bnn.content == 'Naturdrogerie 12.15'
        assert bnn.currency == 'EUR'
        assert bnn.start_date == datetime.date(2015, 11, 30)
        assert bnn.end_date == datetime.date(2016, 1, 31)
        assert bnn.creation_date == datetime.datetime(2015, 11, 23, 16, 44)
        assert bnn.file_counter == 1
        assert bnn.file_counter_next == 99

    def test_errors_occurred(self):
        """Some errors occurred."""
        bnn = Bnn3()
        header = [
            'BNN',
            '3',
            'X',
            "Terra Naturkost Handels KG",
            "T",
            "Naturdrogerie 12.15",
            "EUR",
            'a',
            'b',
            'c',
            '1644',
            'd'
        ]
        errors = _header_to_bnn(header, bnn)

        assert errors == [
            {
                'value': 'X',
                'message': 'Value must be a string in format "45".',
                'row': 'first',
                'column': 3
            },
            {
                'value': 'a',
                'message': 'The date must be a string in format "YYYYMMDD"',
                'row': 'first',
                'column': 8
            },
            {
                'value': 'b',
                'message': 'The date must be a string in format "YYYYMMDD"',
                'row': 'first',
                'column': 9
            },
            {
                'value': ['c', '1644'],
                'message': 'The date must be a list in '
                'format ["YYYYMMDD", "HHMM"]',
                'row': 'first',
                'column': (10, 11)
            },
            {
                'value': 'd',
                'message': 'Value must be a string in format "45".',
                'row': 'first',
                'column': 12
            }
        ]

        assert bnn.identifier == 'BNN'
        assert bnn.version == '3'
        assert not bnn.character_set
        assert bnn.sender_address == 'Terra Naturkost Handels KG'
        assert bnn.scope == 'T'
        assert bnn.content == 'Naturdrogerie 12.15'
        assert bnn.currency == 'EUR'
        assert not bnn.start_date
        assert not bnn.end_date
        assert not bnn.creation_date
        assert not bnn.file_counter


class TestProductsToBnn:
    """Tests for the _products_to_bnn function."""

    def test_no_error(self):
        """No error occurred."""
        bnn = Bnn3()
        product = [
            '514830',
            'A',
            '00000000',
            '0000',
            '4017645003668',
            '4017645403659',
            'Erkltungstee  20x2,1g',
            '',
            '',
            '',
            'FIT',
            '',
            'DE',
            'C%',
            '',
            '0',
            '1311',
            '12',
            '1311',
            '',
            '1',
            '10 x 1 PA',
            '10,000',
            'PA',
            '1',
            'N',
            '',
            '',
            '0',
            '0',
            '0',
            '0',
            '0',
            '1',
            '0',
            '5,95',
            '5,95',
            '3,20',
            'J',
            'N',
            '0',
            '0,00',
            'J',
            'N',
            '0',
            '0,00',
            'J',
            'N',
            '0',
            '0,00',
            'J',
            'N',
            '0',
            '0,00',
            'J',
            'N',
            '0',
            '0',
            'N',
            'N',
            'T',
            '',
            '',
            '',
            '',
            '100 G',
            '2,381',
            '19770303',
            '19770404'
        ]

        assert not _products_to_bnn([product, product], bnn)
        assert bnn.products[0].item_number == '514830'
        assert bnn.products[0].change_code == 'A'
        assert not bnn.products[0].change_date
        assert bnn.products[0].ean_shop == '4017645003668'
        assert bnn.products[0].ean_order == '4017645403659'
        assert bnn.products[0].description == 'Erkltungstee  20x2,1g'
        assert not bnn.products[0].additional_description
        assert not bnn.products[0].label_description
        assert not bnn.products[0].commercial_category
        assert bnn.products[0].brand_bnn_code == 'FIT'
        assert not bnn.products[0].manufacturer_bnn_code
        assert bnn.products[0].country_of_origin == 'DE'
        assert bnn.products[0].quality_label_code == 'C%'
        assert not bnn.products[0].control_body
        assert bnn.products[0].expiry_date_residual_time == 0
        assert bnn.products[0].product_group_bnn == 1311
        assert bnn.products[0].product_group_ifh == 12
        assert bnn.products[0].product_group_vendor == 1311
        assert not bnn.products[0].replacement_item_number
        assert bnn.products[0].minimum_order_quantity == 1.0
        assert bnn.products[0].ordering_unit == '10 x 1 PA'
        assert bnn.products[0].ordering_unit_quantity == 10.0
        assert bnn.products[0].shop_sales_unit == 'PA'
        assert bnn.products[0].quantity_factor == 1.0
        assert bnn.products[0].weighting_item == 'N'
        assert not bnn.products[0].deposit_number_shop_unit
        assert not bnn.products[0].deposit_number_ordering_unit
        assert bnn.products[0].weight_shop_unit == 0.0
        assert bnn.products[0].weight_ordering_unit == 0.0
        assert bnn.products[0].width == 0
        assert bnn.products[0].height == 0
        assert bnn.products[0].depth == 0
        assert bnn.products[0].value_added_tax_code == 1
        assert bnn.products[0].fixed_price_customer == 0.0
        assert bnn.products[0].recommended_price_manufacturer == 5.95
        assert bnn.products[0].recommended_price_vendor == 5.95
        assert bnn.products[0].price == 3.2
        assert bnn.products[0].discountable == 'J'
        assert bnn.products[0].cash_discountable == 'N'
        assert bnn.products[0].scale_quantity_1 == 0.0
        assert bnn.products[0].scale_quantity_1_price == 0.0
        assert bnn.products[0].discountable_1 == 'J'
        assert bnn.products[0].cash_discountable_1 == 'N'
        assert bnn.products[0].scale_quantity_2 == 0.0
        assert bnn.products[0].scale_quantity_2_price == 0.0
        assert bnn.products[0].discountable_2 == 'J'
        assert bnn.products[0].cash_discountable_2 == 'N'
        assert bnn.products[0].scale_quantity_3 == 0.0
        assert bnn.products[0].scale_quantity_3_price == 0.0
        assert bnn.products[0].discountable_3 == 'J'
        assert bnn.products[0].cash_discountable_3 == 'N'
        assert bnn.products[0].scale_quantity_4 == 0.0
        assert bnn.products[0].scale_quantity_4_price == 0.0
        assert bnn.products[0].discountable_4 == 'J'
        assert bnn.products[0].cash_discountable_4 == 'N'
        assert bnn.products[0].scale_quantity_5 == 0.0
        assert bnn.products[0].scale_quantity_5_price == 0.0
        assert bnn.products[0].discountable_5 == 'N'
        assert bnn.products[0].cash_discountable_5 == 'N'
        assert bnn.products[0].item_type == 'T'
        assert not bnn.products[0].special_price
        assert not bnn.products[0].special_price_start_date
        assert not bnn.products[0].special_price_end_date
        assert not bnn.products[0].special_price_recommended_price
        assert bnn.products[0].base_price_unit == '100 G'
        assert bnn.products[0].base_price_factor == 2.381
        assert bnn.products[0].available_from == datetime.date(1977, 3, 3)
        assert bnn.products[0].available_until == datetime.date(1977, 4, 4)

        assert not _products_to_bnn([product], bnn)
        assert bnn.products[1].item_number == '514830'
        assert bnn.products[1].change_code == 'A'
        assert not bnn.products[1].change_date
        assert bnn.products[1].ean_shop == '4017645003668'
        assert bnn.products[1].ean_order == '4017645403659'
        assert bnn.products[1].description == 'Erkltungstee  20x2,1g'
        assert not bnn.products[1].additional_description
        assert not bnn.products[1].label_description
        assert not bnn.products[1].commercial_category
        assert bnn.products[1].brand_bnn_code == 'FIT'
        assert not bnn.products[1].manufacturer_bnn_code
        assert bnn.products[1].country_of_origin == 'DE'
        assert bnn.products[1].quality_label_code == 'C%'
        assert not bnn.products[1].control_body
        assert bnn.products[1].expiry_date_residual_time == 0
        assert bnn.products[1].product_group_bnn == 1311
        assert bnn.products[1].product_group_ifh == 12
        assert bnn.products[1].product_group_vendor == 1311
        assert not bnn.products[1].replacement_item_number
        assert bnn.products[1].minimum_order_quantity == 1.0
        assert bnn.products[1].ordering_unit == '10 x 1 PA'
        assert bnn.products[1].ordering_unit_quantity == 10.0
        assert bnn.products[1].shop_sales_unit == 'PA'
        assert bnn.products[1].quantity_factor == 1.0
        assert bnn.products[1].weighting_item == 'N'
        assert not bnn.products[1].deposit_number_shop_unit
        assert not bnn.products[1].deposit_number_ordering_unit
        assert bnn.products[1].weight_shop_unit == 0.0
        assert bnn.products[1].weight_ordering_unit == 0.0
        assert bnn.products[1].width == 0
        assert bnn.products[1].height == 0
        assert bnn.products[1].depth == 0
        assert bnn.products[1].value_added_tax_code == 1
        assert bnn.products[1].fixed_price_customer == 0.0
        assert bnn.products[1].recommended_price_manufacturer == 5.95
        assert bnn.products[1].recommended_price_vendor == 5.95
        assert bnn.products[1].price == 3.2
        assert bnn.products[1].discountable == 'J'
        assert bnn.products[1].cash_discountable == 'N'
        assert bnn.products[1].scale_quantity_1 == 0.0
        assert bnn.products[1].scale_quantity_1_price == 0.0
        assert bnn.products[1].discountable_1 == 'J'
        assert bnn.products[1].cash_discountable_1 == 'N'
        assert bnn.products[1].scale_quantity_2 == 0.0
        assert bnn.products[1].scale_quantity_2_price == 0.0
        assert bnn.products[1].discountable_2 == 'J'
        assert bnn.products[1].cash_discountable_2 == 'N'
        assert bnn.products[1].scale_quantity_3 == 0.0
        assert bnn.products[1].scale_quantity_3_price == 0.0
        assert bnn.products[1].discountable_3 == 'J'
        assert bnn.products[1].cash_discountable_3 == 'N'
        assert bnn.products[1].scale_quantity_4 == 0.0
        assert bnn.products[1].scale_quantity_4_price == 0.0
        assert bnn.products[1].discountable_4 == 'J'
        assert bnn.products[1].cash_discountable_4 == 'N'
        assert bnn.products[1].scale_quantity_5 == 0.0
        assert bnn.products[1].scale_quantity_5_price == 0.0
        assert bnn.products[1].discountable_5 == 'N'
        assert bnn.products[1].cash_discountable_5 == 'N'
        assert bnn.products[1].item_type == 'T'
        assert not bnn.products[1].special_price
        assert not bnn.products[1].special_price_start_date
        assert not bnn.products[1].special_price_end_date
        assert not bnn.products[1].special_price_recommended_price
        assert bnn.products[1].base_price_unit == '100 G'
        assert bnn.products[1].base_price_factor == 2.381
        assert bnn.products[1].available_from == datetime.date(1977, 3, 3)
        assert bnn.products[1].available_until == datetime.date(1977, 4, 4)

    def test_errors_occurred(self):
        """Some errors occurred."""
        bnn = Bnn3()
        product = [
            '514830',
            'A',
            '00000000',
            '0000',
            '4017645003668',
            '4017645403659',
            'Erkltungstee  20x2,1g',
            '',
            '',
            '',
            'FIT',
            '',
            'DE',
            'C%',
            '',
            '0',
            '1311',
            '12',
            '1311',
            '',
            '1',
            '10 x 1 PA',
            '10,000',
            'PA',
            '1',
            'N',
            '',
            '',
            '0',
            '0',
            '0',
            '0',
            '0',
            '1',
            '0',
            '5,95',
            '5,95',
            '3,20',
            'J',
            'N',
            '0',
            '0,00',
            'J',
            'N',
            '0',
            '0,00',
            'J',
            'N',
            '0',
            '0,00',
            'J',
            'N',
            '0',
            '0,00',
            'J',
            'N',
            '0',
            '0',
            'N',
            'N',
            'T',
            '',
            '',
            '',
            '',
            '100 G',
            '2:381',
            'd',
            '19770404'
        ]

        errors = _products_to_bnn([product, product], bnn)

        assert errors == [
            {
                'value': '2:381',
                'message': 'Value must be a string in format "45" or "45,3".',
                'row': 2,
                'column': 67
            },
            {
                'value': 'd',
                'message': 'The date must be a string in format "YYYYMMDD"',
                'row': 2,
                'column': 68},
            {
                'value': '2:381',
                'message': 'Value must be a string in format "45" or "45,3".',
                'row': 3,
                'column': 67
            },
            {
                'value': 'd',
                'message': 'The date must be a string in format "YYYYMMDD"',
                'row': 3,
                'column': 68
            }
        ]

        assert not bnn.products[0].base_price_factor
        assert not bnn.products[0].available_from
        assert bnn.products[0].available_until == datetime.date(1977, 4, 4)

        assert not bnn.products[1].base_price_factor
        assert not bnn.products[1].available_from
        assert bnn.products[1].available_until == datetime.date(1977, 4, 4)


class TestFooterToBnn:
    """Tests for the _footer_to_bnn function."""

    def test_no_error(self):
        """No error occurred."""
        bnn = Bnn3()
        header = ['', '', '2']

        assert not _footer_to_bnn(header, bnn)
        assert bnn.file_counter_next == 2

    def test_error_occurred(self):
        """A errors occurred."""
        bnn = Bnn3()
        header = ['', '', 'X']
        errors = _footer_to_bnn(header, bnn)

        assert errors == [
            {
                'value': 'X',
                'message': 'Value must be a string in format "45".',
                'row': 'last',
                'column': 3
            }
        ]

        assert not bnn.file_counter_next


class TestBnnFileRead():
    """Tests for the read function."""

    def test_terra_header_and_footer(self, terra_bnn_file):
        """Read the header und footer of a Terra BNN3 file."""
        with BnnFile(terra_bnn_file, 'r') as bnn_file:
            bnn, errors = bnn_file.read()

        assert bnn.identifier == 'BNN'
        assert bnn.version == '3'
        assert bnn.character_set == 0
        assert bnn.sender_address == 'Terra Naturkost Handels KG'
        assert bnn.scope == 'T'
        assert bnn.content == 'Naturdrogerie 12.15'
        assert bnn.currency == 'EUR'
        assert bnn.start_date == datetime.date(2015, 11, 30)
        assert bnn.end_date == datetime.date(2016, 1, 31)
        assert bnn.creation_date == datetime.datetime(2015, 11, 23, 16, 44)
        assert bnn.file_counter == 1
        assert bnn.file_counter_next == 99

        assert not errors

    def test_rapunzel_header_and_footer(self, rapunzel_bnn_file):
        """Read the header und footer of a Rapunzel BNN3 file."""
        with BnnFile(rapunzel_bnn_file, 'r') as bnn_file:
            bnn, errors = bnn_file.read()

        assert bnn.identifier == 'BNN'
        assert bnn.version == '3'
        assert bnn.character_set == 0
        assert bnn.sender_address == 'Rapunzel Naturkost AG,87764 Legau'
        assert bnn.scope == 'V'
        assert bnn.content == 'PREISLITE AB 18.01.2016'
        assert bnn.currency == 'EUR'
        assert bnn.start_date == datetime.date(2016, 1, 18)
        assert bnn.end_date == datetime.date(2017, 3, 1)
        assert bnn.creation_date == datetime.datetime(2016, 1, 18, 14, 31)
        assert bnn.file_counter == 1
        assert bnn.file_counter_next == 99

        assert not errors

    def test_pural_header_and_footer(self, pural_bnn_file):
        """Read the header und footer of a Pural BNN3 file."""
        with BnnFile(pural_bnn_file, 'r') as bnn_file:
            bnn, errors = bnn_file.read()

        assert bnn.identifier == 'BNN'
        assert bnn.version == '3'
        assert bnn.character_set == 1
        assert bnn.sender_address == 'Claus/Pural GmbH'
        assert bnn.scope == 'V'
        assert bnn.content == 'Artikelpflege.de Preisliste'
        assert bnn.currency == 'EUR'
        assert bnn.start_date == datetime.date(2017, 1, 1)
        assert not bnn.end_date
        assert bnn.creation_date == datetime.datetime(2017, 1, 1, 1, 18)
        assert bnn.file_counter == 1
        assert bnn.file_counter_next == 99

        assert errors

    def test_file_not_found(self, tmp_path):
        """A FileNotFoundError was raised."""
        with pytest.raises(FileNotFoundError):
            with BnnFile(tmp_path/'test_file.bnn', 'r') as bnn_file:
                bnn_file.read()

    def test_file_format_error(self, tmp_path):
        """A FileFormatError was raised."""
        test_file = tmp_path/'test_file.bnn'
        test_file.write_text('')

        with pytest.raises(bnn_ex.FileFormatError):
            with BnnFile(tmp_path/'test_file.bnn', 'r') as bnn_file:
                bnn_file.read()

    def test_read_product_of_terra_file(self, terra_bnn_file):
        """Read products from BNN file of Terra Naturkost."""
        with BnnFile(terra_bnn_file, 'r') as bnn_file:
            # bnn = bnn_file.read()
            bnn, errors = bnn_file.read()
            product = bnn.products[0]

            assert product.item_number == '514830'
            assert product.change_code == 'A'
            assert not product.change_date
            assert product.ean_shop == '4017645003668'
            assert product.ean_order == '4017645403659'
            assert product.description == 'Erk\x84ltungstee  20x2,1g'
            assert not product.additional_description
            assert not product.label_description
            assert not product.commercial_category
            assert product.brand_bnn_code == 'FIT'
            assert not product.manufacturer_bnn_code
            assert product.country_of_origin == 'DE'
            assert product.quality_label_code == 'C%'
            assert not product.control_body
            assert product.expiry_date_residual_time == 0
            assert product.product_group_bnn == 1311
            assert product.product_group_ifh == 12
            assert product.product_group_vendor == 1311
            assert not product.replacement_item_number
            assert product.minimum_order_quantity == 1.0
            assert product.ordering_unit == '10 x 1 PA'
            assert product.ordering_unit_quantity == 10.0
            assert product.shop_sales_unit == 'PA'
            assert product.quantity_factor == 1.0
            assert product.weighting_item == 'N'
            assert not product.deposit_number_shop_unit
            assert not product.deposit_number_ordering_unit
            assert product.weight_shop_unit == 0.0
            assert product.weight_ordering_unit == 0.0
            assert product.width == 0
            assert product.height == 0
            assert product.depth == 0
            assert product.value_added_tax_code == 1
            assert product.fixed_price_customer == 0.0
            assert product.recommended_price_manufacturer == 5.95
            assert product.recommended_price_vendor == 5.95
            assert product.price == 3.2
            assert product.discountable == 'J'
            assert product.cash_discountable == 'N'
            assert product.scale_quantity_1 == 0.0
            assert product.scale_quantity_1_price == 0.0
            assert product.discountable_1 == 'J'
            assert product.cash_discountable_1 == 'N'
            assert product.scale_quantity_2 == 0.0
            assert product.scale_quantity_2_price == 0.0
            assert product.discountable_2 == 'J'
            assert product.cash_discountable_2 == 'N'
            assert product.scale_quantity_3 == 0.0
            assert product.scale_quantity_3_price == 0.0
            assert product.discountable_3 == 'J'
            assert product.cash_discountable_3 == 'N'
            assert product.scale_quantity_4 == 0.0
            assert product.scale_quantity_4_price == 0.0
            assert product.discountable_4 == 'J'
            assert product.cash_discountable_4 == 'N'
            assert product.scale_quantity_5 == 0.0
            assert product.scale_quantity_5_price == 0.0
            assert product.discountable_5 == 'N'
            assert product.cash_discountable_5 == 'N'
            assert product.item_type == 'T'
            assert not product.special_price
            assert not product.special_price_start_date
            assert not product.special_price_end_date
            assert not product.special_price_recommended_price
            assert product.base_price_unit == '100 G'
            assert product.base_price_factor == 2.381
            assert product.available_from == datetime.date(1977, 3, 3)
            assert product.available_until == datetime.date(1977, 4, 4)

            assert not errors

    def test_read_product_of_rapunzel_file(self, rapunzel_bnn_file):
        """Read products from BNN file of Rapunzel."""
        with BnnFile(rapunzel_bnn_file, 'r') as bnn_file:
            bnn, errors = bnn_file.read()
            product = bnn.products[0]

            assert product.item_number == '110100'
            assert product.change_code == 'A'
            assert not product.change_date
            assert product.ean_shop == '4006040000112'
            assert product.ean_order == '4006040000105'
            assert product.description == 'Erdnussmus fein'
            assert not product.additional_description
            assert not product.label_description
            assert not product.commercial_category
            assert product.brand_bnn_code == 'RAP'
            assert not product.manufacturer_bnn_code
            assert product.country_of_origin == 'DE'
            assert product.quality_label_code == 'BIO'
            assert product.control_body == 'ABCERT'
            assert product.expiry_date_residual_time == 0
            assert product.product_group_bnn == 721
            assert not product.product_group_ifh
            assert product.product_group_vendor == 1010
            assert not product.replacement_item_number
            assert not product.minimum_order_quantity
            assert product.ordering_unit == '6 x 250g'
            assert product.ordering_unit_quantity == 6.0
            assert product.shop_sales_unit == '250 g'
            assert product.quantity_factor == 1.0
            assert not product.weighting_item
            assert not product.deposit_number_shop_unit
            assert not product.deposit_number_ordering_unit
            assert product.weight_shop_unit == 0.0
            assert product.weight_ordering_unit == 2.58
            assert product.width == 0
            assert product.height == 0
            assert product.depth == 0
            assert product.value_added_tax_code == 1
            assert not product.fixed_price_customer
            assert product.recommended_price_manufacturer == 2.99
            assert product.recommended_price_vendor == 2.99
            assert product.price == 1.84
            assert product.discountable == 'J'
            assert product.cash_discountable == 'N'
            assert not product.scale_quantity_1
            assert not product.scale_quantity_1_price
            assert product.discountable_1 == 'J'
            assert product.cash_discountable_1 == 'N'
            assert not product.scale_quantity_2
            assert not product.scale_quantity_2_price
            assert product.discountable_2 == 'J'
            assert product.cash_discountable_2 == 'N'
            assert not product.scale_quantity_3
            assert not product.scale_quantity_3_price
            assert product.discountable_3 == 'J'
            assert product.cash_discountable_3 == 'N'
            assert not product.scale_quantity_4
            assert not product.scale_quantity_4_price
            assert product.discountable_4 == 'J'
            assert product.cash_discountable_4 == 'N'
            assert not product.scale_quantity_5
            assert not product.scale_quantity_5_price
            assert product.discountable_5 == 'J'
            assert product.cash_discountable_5 == 'N'
            assert product.item_type == 'T'
            assert not product.special_price
            assert not product.special_price_start_date
            assert not product.special_price_end_date
            assert not product.special_price_recommended_price
            assert product.base_price_unit == '100 g'
            assert product.base_price_factor == 0.4
            assert not product.available_from
            assert not product.available_until

            assert not errors

    def test_read_product_of_pural_file(self, pural_bnn_file):
        """Read products from BNN file of Pural."""
        with BnnFile(pural_bnn_file, 'r') as bnn_file:
            bnn, errors = bnn_file.read()

            product = bnn.products[0]

            assert product.item_number == '20132040'
            assert product.change_code == 'A'
            assert product.change_date == datetime.datetime(2016, 3, 3, 19, 46)
            assert not product.ean_shop
            assert not product.ean_order
            assert product.description == 'MÖHREN gew.           Salzmann kg'
            assert product.additional_description == 'Salzmann'
            assert product.label_description == \
                'MÖHREN gew.           Salzmann kg'
            assert not product.commercial_category
            assert not product.brand_bnn_code
            assert not product.manufacturer_bnn_code
            assert product.country_of_origin == 'D'
            assert product.quality_label_code == 'DB'
            assert not product.control_body
            assert product.expiry_date_residual_time == 0
            assert product.product_group_bnn == 0
            assert product.product_group_ifh == 0
            assert product.product_group_vendor == 0
            assert not product.replacement_item_number
            assert product.minimum_order_quantity == 1
            assert product.ordering_unit == '10 x kg'
            assert product.ordering_unit_quantity == 10.0
            assert product.shop_sales_unit == 'kg'
            assert product.quantity_factor == 1.0
            assert product.weighting_item == 'N'
            assert product.deposit_number_shop_unit == '0'
            assert product.deposit_number_ordering_unit == '0'
            assert product.weight_shop_unit == 0.0
            assert product.weight_ordering_unit == 0.0
            assert product.width == 0
            assert product.height == 0
            assert product.depth == 0
            assert product.value_added_tax_code == 1
            assert product.fixed_price_customer == 0.0
            assert product.recommended_price_manufacturer == 0.0
            assert product.recommended_price_vendor == 0.0
            assert product.price == 1.44
            assert product.discountable == 'J'
            assert product.cash_discountable == 'N'
            assert product.scale_quantity_1 == 20.0
            assert product.scale_quantity_1_price == 1.39
            assert product.discountable_1 == 'J'
            assert product.cash_discountable_1 == 'N'
            assert not product.scale_quantity_2
            assert not product.scale_quantity_2_price
            assert not product.discountable_2
            assert not product.cash_discountable_2
            assert not product.scale_quantity_3
            assert not product.scale_quantity_3_price
            assert not product.discountable_3
            assert not product.cash_discountable_3
            assert not product.scale_quantity_4
            assert not product.scale_quantity_4_price
            assert not product.discountable_4
            assert not product.cash_discountable_4
            assert not product.scale_quantity_5
            assert not product.scale_quantity_5_price
            assert not product.discountable_5
            assert not product.cash_discountable_5
            assert product.item_type == 'T'
            assert not product.special_price
            assert not product.special_price_start_date
            assert not product.special_price_end_date
            assert not product.special_price_recommended_price
            assert not product.base_price_unit
            assert product.base_price_factor == 0.0
            assert not product.available_from
            assert not product.available_until

            assert errors


class TestBnnFileWrite():
    """Tests for the write method."""

    def test_correct_file(self, tmp_path, bnn, product):
        """The file was written correctly."""
        filename = tmp_path/'test.bnn'

        bnn.character_set == 0
        bnn.sender_address == 'Test address'
        bnn.scope == 'T'
        bnn.content == 'Price list'
        bnn.currency == 'EUR'
        bnn.start_date == datetime.date(2015, 11, 30)
        bnn.end_date == datetime.date(2016, 1, 31)
        bnn.creation_date == datetime.datetime(2015, 11, 23, 16, 44)
        bnn.file_counter == 1
        bnn.file_counter_next == 99

        product.item_number = '12345'
        product.change_code = 'A'
        product.change_date = datetime.datetime(2015, 11, 23, 16, 44)
        product.ean_shop = '4006040000112'
        product.ean_order = '4006040000105'
        product.description = 'some description'
        product.additional_description = 'additional description'
        product.label_description = 'label description'
        product.commercial_category = 'FFF'
        product.brand_bnn_code = 'FGH'
        product.manufacturer_bnn_code = 'GGGG'
        product.country_of_origin = 'DE'
        product.quality_label_code = 'BIO'
        product.control_body = 'BIO'
        product.expiry_date_residual_time = 0
        product.product_group_bnn = 333
        product.product_group_ifh = 22
        product.product_group_vendor = 1010
        product.replacement_item_number = '23456'
        product.minimum_order_quantity = 34.5
        product.ordering_unit = '6 x 250g'
        product.ordering_unit_quantity = 6.0
        product.shop_sales_unit = '250 g'
        product.quantity_factor = 1.0
        product.weighting_item = 'J'
        product.deposit_number_shop_unit = '2345'
        product.deposit_number_ordering_unit = '5667'
        product.weight_shop_unit = 0.0
        product.weight_ordering_unit = 2.58
        product.width = 0
        product.height = 0
        product.depth = 0
        product.value_added_tax_code = 1
        product.fixed_price_customer = 45.45
        product.recommended_price_manufacturer = 2.99
        product.recommended_price_vendor = 2.99
        product.price = 1.84
        product.discountable = 'J'
        product.cash_discountable = 'N'
        product.scale_quantity_1 = None
        product.scale_quantity_1_price = None
        product.discountable_1 = 'J'
        product.cash_discountable_1 = 'N'
        product.scale_quantity_2 = None
        product.scale_quantity_2_price = None
        product.discountable_2 = 'J'
        product.cash_discountable_2 = 'N'
        product.scale_quantity_3 = None
        product.scale_quantity_3_price = None
        product.discountable_3 = 'J'
        product.cash_discountable_3 = 'N'
        product.scale_quantity_4 = None
        product.scale_quantity_4_price = None
        product.discountable_4 = 'J'
        product.cash_discountable_4 = 'N'
        product.scale_quantity_5 = None
        product.scale_quantity_5_price = None
        product.discountable_5 = 'J'
        product.cash_discountable_5 = 'N'
        product.item_type = 'T'
        product.special_price = 'A'
        product.special_price_start_date = datetime.date(2012, 2, 4)
        product.special_price_end_date = datetime.date(2012, 3, 3)
        product.special_price_recommended_price = 2.34
        product.base_price_unit == '100 g'
        product.base_price_factor == 0.4
        product.available_from = datetime.date(2020, 3, 4)
        product.available_until = datetime.date(2020, 4, 5)

        product.validate()

        bnn.products.add(product)

        with BnnFile(filename, 'w') as bnn_file:
            bnn_file.write(bnn)

        with open(filename, 'r') as bnn_file:
            header = bnn_file.readline()
            product_row = bnn_file.readline()
            footer = bnn_file.readline()

        assert header == 'BNN;3;0;address;T;description;EUR;20210306;' \
            '20210307;20210305;2113;1\n'

        assert product_row == \
            '12345;A;20151123;1644;4006040000112;4006040000105;' \
            'some description;additional description;label description;' \
            'FFF;FGH;GGGG;DE;BIO;BIO;0;333;22;1010;23456;34.5;6 x 250g;' \
            '6;250 g;1;J;2345;5667;0;2.58;0;0;0;1;45.45;2.99;2.99;1.84;' \
            'J;N;;;J;N;;;J;N;;;J;N;;;J;N;;;J;N;T;A;20120204;20120303;' \
            '2.34;2.45;6.3;20200304;20200405\n'

        assert footer == ';;99\n'
