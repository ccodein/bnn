"""Tests for the mapper module."""
from datetime import date, datetime
from bnn import mapper
import pytest


class TestIntType():
    """Tests for the int_type mapper function."""

    def test_correct_int(self):
        """Is int type but inverse arg is False."""
        assert mapper.int_type(28) == '28'

    def test_value_is_none(self):
        """The value is None."""
        assert mapper.int_type(None) == ''

    def test_wrong_type(self):
        """The argument is a float type."""
        with pytest.raises(TypeError):
            mapper.int_type(3.1415)

    def test_empty_string(self):
        """Value is an empty string."""
        assert not mapper.int_type('', inverse=True)

    def test_correct_int_string(self):
        """Is a correct int string."""
        assert isinstance(mapper.int_type('45', inverse=True), int)

    def test_incorrect_int_string_format(self):
        """String format is not a int."""
        with pytest.raises(TypeError):
            mapper.int_type('7s45', inverse=True)


class TestFloatType():
    """Tests for the float_type mapper function."""

    def test_float_without_digit_after(self):
        """Float has no digit after decimal point."""
        assert mapper.float_type(28.0) == '28'

    def test_float_with_digit_after(self):
        """Float has digit after decimal point."""
        assert mapper.float_type(28.1) == '28.1'

    def test_value_is_none(self):
        """The value is None."""
        assert mapper.float_type(None) == ''

    def test_wrong_type(self):
        """The argument is a float type."""
        with pytest.raises(TypeError):
            mapper.float_type(3)

    def test_string_with_digit_after_comma(self):
        """String float has digit after the comma."""
        assert mapper.float_type('45,3', inverse=True) == 45.3

    def test_empty_string(self):
        """Value is an empty string."""
        assert not mapper.float_type('', inverse=True)

    def test_string_without_digit_after_comma(self):
        """String float has no digit after the comma."""
        assert mapper.float_type('45', inverse=True) == 45.0

    def test_incorrect_float_string_format(self):
        """String format is not a float."""
        with pytest.raises(TypeError):
            mapper.float_type('7s45', inverse=True)


class TestStringType():
    """Tests for the string_type mapper function."""

    def test_value_is_none(self):
        """Value is None and inverse arg is False."""
        assert mapper.string_type(None) == ''

    def test_value_is_string(self):
        """Value is string and inverse arg is False."""
        assert mapper.string_type('test') == 'test'

    def test_wrong_type(self):
        """The argument is a float type."""
        with pytest.raises(TypeError):
            mapper.string_type(3.1415)

    def test_value_is_empty_string_inverse(self):
        """Value is None and inverse arg is False."""
        assert not mapper.string_type('', inverse=True)

    def test_value_is_string_inverse(self):
        """Value is string and inverse arg is False."""
        assert mapper.string_type('test', inverse=True) == 'test'


class TestDateType():
    """Tests for the date_type mapper function."""

    def test_not_inverse_and_date(self):
        """Convert Python date to string."""
        assert mapper.date_type(date(2015, 11, 3)) == '20151103'

    def test_none_to_string(self):
        """Convert Python None to string."""
        assert mapper.date_type(None) == ''

    def test_not_inverse_and_wrong_type(self):
        """Wrong date string format and not inverse."""
        with pytest.raises(TypeError):
            mapper.date_type('test')

    def test_inverse_and_string_is_correct_format(self):
        """Convert string date to Python type."""
        assert mapper.date_type('20151130', inverse=True) == \
            date(2015, 11, 30)

    def test_empty_string(self):
        """It is an empty string."""
        assert not mapper.date_type('', inverse=True)


class TestDatetimeType():
    """Tests for the datetime_type mapper function."""

    def test_datetime_to_str(self):
        """Convert Python datetime to string."""
        assert mapper.datetime_type(datetime(2015, 11, 3, 22, 44)) \
            == ['20151103', '2244']

    def test_none_to_string(self):
        """Convert Python None to string."""
        assert mapper.datetime_type(None) == ''

    def test_string_to_datetime(self):
        """Convert string to datetime."""
        time = datetime(2015, 11, 3, 22, 44)

        assert mapper.datetime_type(['20151103', '2244'], inverse=True) == time

    def test_wrong_minute_format(self):
        """Wrong minute format."""
        with pytest.raises(TypeError):
            mapper.datetime_type(['20151103', '2260'], inverse=True)

    def test_wrong_hour_format(self):
        """Wrong hours format."""
        with pytest.raises(TypeError):
            mapper.datetime_type(['20151103', '2544'], inverse=True)

    def test_empty_string(self):
        """It is an empty string."""
        assert not mapper.datetime_type(['', ''], inverse=True)
