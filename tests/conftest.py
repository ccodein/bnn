"""Conftest file for pytest."""
import pytest
from pathlib import Path
from datetime import date, datetime
from bnn.products import Product3
from bnn.bnn import Bnn3


@pytest.fixture
def tests_path():
    """Get the path to the tests folder."""
    return Path(__file__).parent


@pytest.fixture
def terra_bnn_file(tests_path):
    """Get the Terra BNN test file."""
    return tests_path/'data/terra.bnn'


@pytest.fixture
def pural_bnn_file(tests_path):
    """Get the Pural BNN test file."""
    return tests_path/'data/pural.bnn'


@pytest.fixture
def rapunzel_bnn_file(tests_path):
    """Get the Rapunzel BNN test file."""
    return tests_path/'data/rapunzel.bnn'


@pytest.fixture
def bnn():
    """Get a Bnn3 instance."""
    return Bnn3(
        0,
        'address',
        'T',
        'description',
        'EUR',
        date(2021, 3, 6),
        date(2021, 3, 7),
        datetime(2021, 3, 5, 21, 13),
        1,
        99
    )


@pytest.fixture
def product():
    """Get a Product3 instance."""
    return Product3(
        item_number='123456',
        change_code='N',
        description='description',
        brand_bnn_code='ddd',
        country_of_origin='DE',
        quality_label_code='GGG',
        ordering_unit='1x5',
        ordering_unit_quantity=5.1,
        shop_sales_unit='1x5',
        quantity_factor=5.1,
        deposit_number_shop_unit='23456',
        weight_ordering_unit=6.2,
        value_added_tax_code=1,
        price=3.24,
        base_price_unit='2.45',
        base_price_factor=6.3
    )
