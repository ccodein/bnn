"""Test for the utils module."""
import pytest
from bnn import exceptions as bnn_ex
from bnn.validation import max_length_validation, validate


class TestLengthCheck():
    """Tests for the length_check decorator."""

    def test_string_length_less_max(self):
        """String length is less than the maximum length."""
        assert not max_length_validation('ggg', 4)

    def test_string_length_greater_max(self):
        """String length is greater than the maximum length."""
        with pytest.raises(bnn_ex.MaxLengthError):
            max_length_validation('ggg', 2)

    def test_string_length_equal_max(self):
        """String length is equal to the maximum length."""
        assert not max_length_validation('ggg', 3)

    def test_int_length_less_max(self):
        """Integer length is less than the maximum length."""
        assert not max_length_validation(100, 4)

    def test_int_length_equal_max(self):
        """Integer length is equal to the maximum length."""
        assert not max_length_validation(100, 3)

    def test_int_length_greater_max(self):
        """Integer length is greater than the maximum length."""
        with pytest.raises(bnn_ex.MaxLengthError):
            max_length_validation(100, 2)

    def test_float_length_less_max(self):
        """Float length is less than the maximum length."""
        assert not max_length_validation(10.34, (2, 2))

    def test_float_length_before_greater_max(self):
        """Float length before decimal point is greater than the maximum."""
        with pytest.raises(bnn_ex.MaxLengthError):
            max_length_validation(10.34, (1, 2))

    def test_float_length_after_greater_max(self):
        """Float length after decimal point is greater than the maximum."""
        with pytest.raises(bnn_ex.MaxLengthError):
            max_length_validation(10.34, (2, 1))

    def test_error_message(self):
        """Is correct error message."""
        try:
            max_length_validation('test value', 3)

        except bnn_ex.MaxLengthError as e:
            assert str(e) == 'The length of the value is greater than "3".'


class TestValidate():
    """Tests for the validate function."""

    def test_value_none_required_false(self):
        """If value is None all validations except required are skipped."""
        definition = {
                'options': None,
                'max_length': 4,
                'type': int,
                'required': None
                }

        validate(None, definition)

    def test_value_none_required_true(self):
        """If value is None all validations except required are skipped."""
        definition = {
                'options': None,
                'max_length': 4,
                'type': int,
                'required': True
                }

        with pytest.raises(bnn_ex.RequiredError):
            validate(None, definition)

    def test_type_error(self):
        """A TypeError was raised."""
        definition = {
                'options': ['X', 'Y', 'Z'],
                'max_length': None,
                'type': int,
                'required': None
                }

        with pytest.raises(TypeError):
            validate('X', definition)

    def test_correct_value(self):
        """Correct value for definition passed."""
        definition = {
                'options': ['X', 'Y', 'Z'],
                'max_length': None,
                'type': str,
                'required': None
                }

        validate('X', definition)

    def test_options_error(self):
        """A OptionsError was raised."""
        definition = {
                'options': ['X', 'Y', 'Z'],
                'max_length': None,
                'type': str,
                'required': None
                }
        with pytest.raises(bnn_ex.OptionsError):
            validate('A', definition)

    def test_int_length_error(self):
        """A MaxLengthError was raised."""
        definition = {
                'options': None,
                'type': str,
                'max_length': 4,
                'required': None
                }

        with pytest.raises(bnn_ex.MaxLengthError):
            validate('aaaaa', definition)

    def test_skip_max_length(self):
        """The maximal length validation was skiped."""
        definition = {
                'options': None,
                'type': str,
                'max_length': 4,
                'required': None
                }

        validate('aaaaa', definition, skip_max_length=True)

    def test_skip_options(self):
        """The options validation was skiped."""
        definition = {
                'options': ['e', 'f'],
                'type': str,
                'max_length': 4,
                'required': None
                }

        validate('aaaa', definition, skip_options=True)

    def test_skip_type(self):
        """The type validation was skiped."""
        definition = {
                'options': None,
                'type': int,
                'max_length': 4,
                'required': None
                }

        validate('aaaa', definition, skip_type=True)

    def test_skip_required(self):
        """The required validation was skiped."""
        definition = {
                'options': None,
                'type': int,
                'max_length': 4,
                'required': True
                }

        validate(None, definition, skip_required=True)
