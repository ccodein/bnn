"""Tests for the bnn module."""
import pytest
from datetime import date
from bnn.products import Product3, Products


class TestProductsAdd:
    """Tests for the add method of Products class."""

    def test_product_added(self, product):
        """The Product was added."""
        products = Products()
        products.add(product)

        assert products._data[0] == product


class TestProductsLen:
    """Tests for the __len__ method of Products class."""

    def test_correct_len(self, product):
        """The correct length returned."""
        products = Products()
        products.add(product)
        products.add(product)

        assert len(products) == 2


class TestProductsGetitem:
    """Tests for the __getitem__ method of Products class."""

    def test_correct_product(self, product):
        """The correct product was returned."""
        products = Products()
        products.add(product)

        assert products[0] == product


class TestProductsPop:
    """Tests for the pop method of Products class."""

    def test_product_removed_by_index(self, product):
        """The Product was removed by index."""
        product_two = Product3()
        products = Products()
        products.add(product)
        products.add(product_two)

        assert len(products) == 2
        assert products.pop(0) == product
        assert len(products) == 1
        assert products[0] == product_two


class TestProductsRemove:
    """Tests for the remove method of Products class."""

    def test_product_removed_by_value(self, product):
        """The Product was removed by value."""
        product_two = Product3()
        products = Products()
        products.add(product)
        products.add(product_two)

        assert len(products) == 2

        products.remove(product)

        assert len(products) == 1
        assert products[0] == product_two


class TestProduct3ItemNumber:
    """Tests for the item_number property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.item_number

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.item_number = 123
        result = product.validate()

        assert result == [
            {
                'property': 'item_number',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.item_number = 'x'*14

        assert not product.validate()

    def test_max_length_error(self, product):
        """String is too long."""
        product.item_number = 'x'*15
        result = product.validate()

        assert result == [
            {
                'property': 'item_number',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "14" characters.',
            }
        ]


class TestBnn3ChangeCode:
    """Tests for the change_code property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.change_code

    @pytest.mark.parametrize('option', ('N', 'A', 'X', 'R', 'V', 'W'))
    def test_set_property(self, option, product):
        """Set the property."""
        product.change_code = option

        assert not product.validate()

    def test_wrong_option(self, product):
        """Is a worng option."""
        product.change_code = 'Z'
        result = product.validate()

        assert result == [
            {
                'property': 'change_code',
                'error': 'OptionsError',
                'message': 'Only the options '
                '"(\'N\', \'A\', \'X\', \'R\', \'V\', \'W\')" are allowed.',
            }
        ]

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.change_code = 123
        result = product.validate()

        assert result == [
            {
                'property': 'change_code',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]


class TestProduct3ChangeDate:
    """Tests for the change_date property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.change_date

    def test_set_property(self, product):
        """Set the property."""
        product.change_date = date(2021, 3, 6)

        assert not product.validate()

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.change_date = 123
        result = product.validate()

        assert result == [
            {
                'property': 'change_date',
                'error': 'TypeError',
                'message': 'The property must be of "datetime" type.',
            }
        ]


class TestProduct3EanShop:
    """Tests for the ean_shop property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.ean_shop

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.ean_shop = 123
        result = product.validate()

        assert result == [
            {
                'property': 'ean_shop',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.ean_shop = 'x'*14

        assert not product.validate()

    def test_max_length_error(self, product):
        """String is too long."""
        product.ean_shop = 'x'*15
        result = product.validate()

        assert result == [
            {
                'property': 'ean_shop',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "14" characters.',
            }
        ]


class TestProduct3EanOrder:
    """Tests for the ean_order property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.ean_order

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.ean_order = 123
        result = product.validate()

        assert result == [
            {
                'property': 'ean_order',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.ean_order = 'x'*14

        assert not product.validate()

    def test_max_length_error(self, product):
        """String is too long."""
        product.ean_order = 'x'*15
        result = product.validate()

        assert result == [
            {
                'property': 'ean_order',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "14" characters.',
            }
        ]


class TestProduct3Description:
    """Tests for the description property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.description

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.description = 123
        result = product.validate()

        assert result == [
            {
                'property': 'description',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.description = 'x'*50

        assert not product.validate()

    def test_max_length_error(self, product):
        """String is too long."""
        product.description = 'x'*51
        result = product.validate()

        assert result == [
            {
                'property': 'description',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "50" characters.',
            }
        ]


class TestProduct3AdditionalDescription:
    """Tests for the additional_description property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.additional_description

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.additional_description = 123
        result = product.validate()

        assert result == [
            {
                'property': 'additional_description',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.additional_description = 'x'*50

        assert not product.validate()

    def test_max_length_error(self, product):
        """String is too long."""
        product.additional_description = 'x'*51
        result = product.validate()

        assert result == [
            {
                'property': 'additional_description',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "50" characters.',
            }
        ]


class TestProduct3LabelDescription:
    """Tests for the label_description property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.label_description

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.label_description = 123
        result = product.validate()

        assert result == [
            {
                'property': 'label_description',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.label_description = 'x'*30

        assert not product.validate()

    def test_max_length_error(self, product):
        """String is too long."""
        product.label_description = 'x'*31
        result = product.validate()

        assert result == [
            {
                'property': 'label_description',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "30" characters.',
            }
        ]


class TestProduct3Commercial_category:
    """Tests for the commercial_category property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.commercial_category

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.commercial_category = 123
        result = product.validate()

        assert result == [
            {
                'property': 'commercial_category',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.commercial_category = 'x'*5

        assert not product.validate()

    def test_max_length_error(self, product):
        """String is too long."""
        product.commercial_category = 'x'*6
        result = product.validate()

        assert result == [
            {
                'property': 'commercial_category',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "5" characters.',
            }
        ]


class TestProduct3BrandBnnCode:
    """Tests for the brand_bnn_code property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.brand_bnn_code

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.brand_bnn_code = 123
        result = product.validate()

        assert result == [
            {
                'property': 'brand_bnn_code',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.brand_bnn_code = 'x'*3

        assert not product.validate()

    def test_max_length_error(self, product):
        """String is too long."""
        product.brand_bnn_code = 'x'*4
        result = product.validate()

        assert result == [
            {
                'property': 'brand_bnn_code',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "3" characters.',
            }
        ]


class TestProduct3ManufacturerBnnCode:
    """Tests for the manufacturer_bnn_code property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.manufacturer_bnn_code

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.manufacturer_bnn_code = 123
        result = product.validate()

        assert result == [
            {
                'property': 'manufacturer_bnn_code',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.manufacturer_bnn_code = 'x'*4

        assert not product.validate()

    def test_max_length_error(self, product):
        """String is too long."""
        product.manufacturer_bnn_code = 'x'*5
        result = product.validate()

        assert result == [
            {
                'property': 'manufacturer_bnn_code',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "4" characters.',
            }
        ]


class TestProduct3CountryOfOrigin:
    """Tests for the country_of_origin property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.country_of_origin

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.country_of_origin = 123
        result = product.validate()

        assert result == [
            {
                'property': 'country_of_origin',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.country_of_origin = 'x'*3

        assert not product.validate()

    def test_max_length_error(self, product):
        """String is too long."""
        product.country_of_origin = 'x'*4
        result = product.validate()

        assert result == [
            {
                'property': 'country_of_origin',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "3" characters.',
            }
        ]


class TestProduct3QualityLabelCode:
    """Tests for the quality_label_code property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.quality_label_code

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.quality_label_code = 123
        result = product.validate()

        assert result == [
            {
                'property': 'quality_label_code',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.quality_label_code = 'x'*4

        assert not product.validate()

    def test_max_length_error(self, product):
        """String is too long."""
        product.quality_label_code = 'x'*5
        result = product.validate()

        assert result == [
            {
                'property': 'quality_label_code',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "4" characters.',
            }
        ]


class TestProduct3ControlBody:
    """Tests for the control_body property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.control_body

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.control_body = 123
        result = product.validate()

        assert result == [
            {
                'property': 'control_body',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.control_body = 'x'*15

        assert not product.validate()

    def test_max_length_error(self, product):
        """String is too long."""
        product.control_body = 'x'*16
        result = product.validate()

        assert result == [
            {
                'property': 'control_body',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "15" characters.',
            }
        ]


class TestProduct3ExpiryDateResidualTime:
    """Tests for the expiry_date_residual_time property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.expiry_date_residual_time

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.expiry_date_residual_time = 'xyz'
        result = product.validate()

        assert result == [
            {
                'property': 'expiry_date_residual_time',
                'error': 'TypeError',
                'message': 'The property must be of "int" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.expiry_date_residual_time = 9999

        assert not product.validate()

    def test_max_length_error(self, product):
        """Integer is too long."""
        product.expiry_date_residual_time = 10000
        result = product.validate()

        assert result == [
            {
                'property': 'expiry_date_residual_time',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "4" characters.',
            }
        ]


class TestProduct3ProductGroupBnn:
    """Tests for the product_group_bnn property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.product_group_bnn

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.product_group_bnn = 'xyz'
        result = product.validate()

        assert result == [
            {
                'property': 'product_group_bnn',
                'error': 'TypeError',
                'message': 'The property must be of "int" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.product_group_bnn = 9999

        assert not product.validate()

    def test_max_length_error(self, product):
        """Integer is too long."""
        product.product_group_bnn = 10000
        result = product.validate()

        assert result == [
            {
                'property': 'product_group_bnn',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "4" characters.',
            }
        ]


class TestProduct3ProductGroupIfh:
    """Tests for the product_group_ifh property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.product_group_ifh

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.product_group_ifh = 'xyz'
        result = product.validate()

        assert result == [
            {
                'property': 'product_group_ifh',
                'error': 'TypeError',
                'message': 'The property must be of "int" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.product_group_ifh = 99

        assert not product.validate()

    def test_max_length_error(self, product):
        """Integer is too long."""
        product.product_group_ifh = 100
        result = product.validate()

        assert result == [
            {
                'property': 'product_group_ifh',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "2" characters.',
            }
        ]


class TestProduct3ProductGroupVendor:
    """Tests for the product_group_vendor property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.product_group_vendor

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.product_group_vendor = 'xyz'
        result = product.validate()

        assert result == [
            {
                'property': 'product_group_vendor',
                'error': 'TypeError',
                'message': 'The property must be of "int" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.product_group_vendor = 9999

        assert not product.validate()

    def test_max_length_error(self, product):
        """Integer is too long."""
        product.product_group_vendor = 10000
        result = product.validate()

        assert result == [
            {
                'property': 'product_group_vendor',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "4" characters.',
            }
        ]


class TestProduct3ReplacementItemNumber:
    """Tests for the replacement_item_number property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.replacement_item_number

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.replacement_item_number = 123
        result = product.validate()

        assert result == [
            {
                'property': 'replacement_item_number',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.replacement_item_number = 'x'*14

        assert not product.validate()

    def test_max_length_error(self, product):
        """String is too long."""
        product.replacement_item_number = 'x'*15
        result = product.validate()

        assert result == [
            {
                'property': 'replacement_item_number',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "14" characters.',
            }
        ]


class TestProduct3MinimumOrderQuantity:
    """Tests for the minimum_order_quantity property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.minimum_order_quantity

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.minimum_order_quantity = 'xyz'
        result = product.validate()

        assert result == [
            {
                'property': 'minimum_order_quantity',
                'error': 'TypeError',
                'message': 'The property must be of "float" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.minimum_order_quantity = 99999.999

        assert not product.validate()

    def test_max_length_error_before_decimal(self, product):
        """Float is too long before decimal point."""
        product.minimum_order_quantity = 100000.999
        result = product.validate()

        assert result == [
            {
                'property': 'minimum_order_quantity',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 3)" characters.',
            }
        ]

    def test_max_length_error_after_decimal(self, product):
        """Float is too long after decimal point."""
        product.minimum_order_quantity = 99999.1001
        result = product.validate()

        assert result == [
            {
                'property': 'minimum_order_quantity',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 3)" characters.',
            }
        ]


class TestProduct3OrderingUnit:
    """Tests for the ordering_unit property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.ordering_unit

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.ordering_unit = 123
        result = product.validate()

        assert result == [
            {
                'property': 'ordering_unit',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.ordering_unit = 'x'*15

        assert not product.validate()

    def test_max_length_error(self, product):
        """String is too long."""
        product.ordering_unit = 'x'*16
        result = product.validate()

        assert result == [
            {
                'property': 'ordering_unit',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "15" characters.',
            }
        ]


class TestProduct3OrderingUnitQuantity:
    """Tests for the ordering_unit_quantity property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.ordering_unit_quantity

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.ordering_unit_quantity = 'xyz'
        result = product.validate()

        assert result == [
            {
                'property': 'ordering_unit_quantity',
                'error': 'TypeError',
                'message': 'The property must be of "float" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.ordering_unit_quantity = 99999.999

        assert not product.validate()

    def test_max_length_error_before_decimal(self, product):
        """Float is too long before decimal point."""
        product.ordering_unit_quantity = 100000.999
        result = product.validate()

        assert result == [
            {
                'property': 'ordering_unit_quantity',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 3)" characters.',
            }
        ]

    def test_max_length_error_after_decimal(self, product):
        """Float is too long after decimal point."""
        product.ordering_unit_quantity = 99999.1001
        result = product.validate()

        assert result == [
            {
                'property': 'ordering_unit_quantity',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 3)" characters.',
            }
        ]


class TestProduct3ShopSalesUnit:
    """Tests for the shop_sales_unit property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.shop_sales_unit

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.shop_sales_unit = 123
        result = product.validate()

        assert result == [
            {
                'property': 'shop_sales_unit',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.shop_sales_unit = 'x'*10

        assert not product.validate()

    def test_max_length_error(self, product):
        """String is too long."""
        product.shop_sales_unit = 'x'*11
        result = product.validate()

        assert result == [
            {
                'property': 'shop_sales_unit',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "10" characters.',
            }
        ]


class TestProduct3QuantityFactor:
    """Tests for the quantity_factor property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.quantity_factor

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.quantity_factor = 'xyz'
        result = product.validate()

        assert result == [
            {
                'property': 'quantity_factor',
                'error': 'TypeError',
                'message': 'The property must be of "float" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.quantity_factor = 99999.999

        assert not product.validate()

    def test_max_length_error_before_decimal(self, product):
        """Float is too long before decimal point."""
        product.quantity_factor = 100000.999
        result = product.validate()

        assert result == [
            {
                'property': 'quantity_factor',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 3)" characters.',
            }
        ]

    def test_max_length_error_after_decimal(self, product):
        """Float is too long after decimal point."""
        product.quantity_factor = 99999.1001
        result = product.validate()

        assert result == [
            {
                'property': 'quantity_factor',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 3)" characters.',
            }
        ]


class TestBnn3WeightingItem():
    """Tests for the weighting_item property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.weighting_item

    @pytest.mark.parametrize('option', ('N', 'J'))
    def test_set_property(self, product, option):
        """Set the property."""
        product.weighting_item = option

        assert not product.validate()

    def test_wrong_option(self, product):
        """Is a worng option."""
        product.weighting_item = 'Z'
        result = product.validate()

        assert result == [
            {
                'property': 'weighting_item',
                'error': 'OptionsError',
                'message': 'Only the options "(\'J\', \'N\')" are allowed.',
            }
        ]

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.weighting_item = 123
        result = product.validate()

        assert result == [
            {
                'property': 'weighting_item',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]


class TestProduct3DepositNumberShopUnit:
    """Tests for the deposit_number_shop_unit property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.deposit_number_shop_unit

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.deposit_number_shop_unit = 123
        result = product.validate()

        assert result == [
            {
                'property': 'deposit_number_shop_unit',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.deposit_number_shop_unit = 'x'*10

        assert not product.validate()

    def test_max_length_error(self, product):
        """String is too long."""
        product.deposit_number_shop_unit = 'x'*11
        result = product.validate()

        assert result == [
            {
                'property': 'deposit_number_shop_unit',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "10" characters.',
            }
        ]


class TestProduct3DepositNumberOrderingUnit:
    """Tests for the deposit_number_ordering_unit property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.deposit_number_ordering_unit

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.deposit_number_ordering_unit = 123
        result = product.validate()

        assert result == [
            {
                'property': 'deposit_number_ordering_unit',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.deposit_number_ordering_unit = 'x'*10

        assert not product.validate()

    def test_max_length_error(self, product):
        """String is too long."""
        product.deposit_number_ordering_unit = 'x'*11
        result = product.validate()

        assert result == [
            {
                'property': 'deposit_number_ordering_unit',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "10" characters.',
            }
        ]


class TestProduct3WeightShopUnit:
    """Tests for the weight_shop_unit property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.weight_shop_unit

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.weight_shop_unit = 'xyz'
        result = product.validate()

        assert result == [
            {
                'property': 'weight_shop_unit',
                'error': 'TypeError',
                'message': 'The property must be of "float" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.weight_shop_unit = 99999.999

        assert not product.validate()

    def test_max_length_error_before_decimal(self, product):
        """Float is too long before decimal point."""
        product.weight_shop_unit = 100000.999
        result = product.validate()

        assert result == [
            {
                'property': 'weight_shop_unit',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 3)" characters.',
            }
        ]

    def test_max_length_error_after_decimal(self, product):
        """Float is too long after decimal point."""
        product.weight_shop_unit = 99999.1001
        result = product.validate()

        assert result == [
            {
                'property': 'weight_shop_unit',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 3)" characters.',
            }
        ]


class TestProduct3WeightOrderingUnit:
    """Tests for the weight_ordering_unit property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.weight_ordering_unit

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.weight_ordering_unit = 'xyz'
        result = product.validate()

        assert result == [
            {
                'property': 'weight_ordering_unit',
                'error': 'TypeError',
                'message': 'The property must be of "float" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.weight_ordering_unit = 99999.999

        assert not product.validate()

    def test_max_length_error_before_decimal(self, product):
        """Float is too long before decimal point."""
        product.weight_ordering_unit = 100000.999
        result = product.validate()

        assert result == [
            {
                'property': 'weight_ordering_unit',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 3)" characters.',
            }
        ]

    def test_max_length_error_after_decimal(self, product):
        """Float is too long after decimal point."""
        product.weight_ordering_unit = 99999.1001
        result = product.validate()

        assert result == [
            {
                'property': 'weight_ordering_unit',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 3)" characters.',
            }
        ]


class TestProduct3Width:
    """Tests for the width property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.width

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.width = 'xyz'
        result = product.validate()

        assert result == [
            {
                'property': 'width',
                'error': 'TypeError',
                'message': 'The property must be of "int" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.width = 999

        assert not product.validate()

    def test_max_length_error(self, product):
        """Integer is too long."""
        product.width = 1000
        result = product.validate()

        assert result == [
            {
                'property': 'width',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "3" characters.',
            }
        ]


class TestProduct3Height:
    """Tests for the height property."""

    def test_check_default(self, product):
        """Check the default property."""
        product = Product3()

        assert not product.height

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.height = 'xyz'
        result = product.validate()

        assert result == [
            {
                'property': 'height',
                'error': 'TypeError',
                'message': 'The property must be of "int" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.height = 999

        assert not product.validate()

    def test_max_length_error(self, product):
        """Integer is too long."""
        product.height = 1000
        result = product.validate()

        assert result == [
            {
                'property': 'height',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "3" characters.',
            }
        ]


class TestProduct3Depth:
    """Tests for the depth property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.depth

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.depth = 'xyz'
        result = product.validate()

        assert result == [
            {
                'property': 'depth',
                'error': 'TypeError',
                'message': 'The property must be of "int" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.depth = 999

        assert not product.validate()

    def test_max_length_error(self, product):
        """Integer is too long."""
        product.depth = 1000
        result = product.validate()

        assert result == [
            {
                'property': 'depth',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "3" characters.',
            }
        ]


class TestProduct3ValueAddedTaxCode:
    """Tests for the value_added_tax_code property."""

    def test_check_default(self, product):
        """Check the default property."""
        product = Product3()

        assert not product.value_added_tax_code

    @pytest.mark.parametrize('option', (1, 2, 3))
    def test_set_property(self, product, option):
        """Set the property."""
        product.value_added_tax_code = option

        assert not product.validate()

    def test_wrong_option(self, product):
        """Is a worng option."""
        product.value_added_tax_code = 4
        result = product.validate()

        assert result == [
            {
                'property': 'value_added_tax_code',
                'error': 'OptionsError',
                'message': 'Only the options "(1, 2, 3)" are allowed.',
            }
        ]

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.value_added_tax_code = 'x'
        result = product.validate()

        assert result == [
            {
                'property': 'value_added_tax_code',
                'error': 'TypeError',
                'message': 'The property must be of "int" type.',
            }
        ]


class TestProduct3FixedPriceCustomer:
    """Tests for the fixed_price_customer property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.fixed_price_customer

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.fixed_price_customer = 'xyz'
        result = product.validate()

        assert result == [
            {
                'property': 'fixed_price_customer',
                'error': 'TypeError',
                'message': 'The property must be of "float" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.fixed_price_customer = 99999.99

        assert not product.validate()

    def test_max_length_error_before_decimal(self, product):
        """Float is too long before decimal point."""
        product.fixed_price_customer = 100000.99
        result = product.validate()

        assert result == [
            {
                'property': 'fixed_price_customer',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 2)" characters.',
            }
        ]

    def test_max_length_error_after_decimal(self, product):
        """Float is too long after decimal point."""
        product.fixed_price_customer = 99999.101
        result = product.validate()

        assert result == [
            {
                'property': 'fixed_price_customer',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 2)" characters.',
            }
        ]


class TestProduct3RecommendedPriceManufacturer:
    """Tests for the recommended_price_manufacturer property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.recommended_price_manufacturer

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.recommended_price_manufacturer = 'xyz'
        result = product.validate()

        assert result == [
            {
                'property': 'recommended_price_manufacturer',
                'error': 'TypeError',
                'message': 'The property must be of "float" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.recommended_price_manufacturer = 99999.99

        assert not product.validate()

    def test_max_length_error_before_decimal(self, product):
        """Float is too long before decimal point."""
        product.recommended_price_manufacturer = 100000.99
        result = product.validate()

        assert result == [
            {
                'property': 'recommended_price_manufacturer',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 2)" characters.',
            }
        ]

    def test_max_length_error_after_decimal(self, product):
        """Float is too long after decimal point."""
        product.recommended_price_manufacturer = 99999.101
        result = product.validate()

        assert result == [
            {
                'property': 'recommended_price_manufacturer',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 2)" characters.',
            }
        ]


class TestProduct3RecommendedPriceVendor:
    """Tests for the recommended_price_vendor property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.recommended_price_vendor

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.recommended_price_vendor = 'xyz'
        result = product.validate()

        assert result == [
            {
                'property': 'recommended_price_vendor',
                'error': 'TypeError',
                'message': 'The property must be of "float" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.recommended_price_vendor = 99999.99

        assert not product.validate()

    def test_max_length_error_before_decimal(self, product):
        """Float is too long before decimal point."""
        product.recommended_price_vendor = 100000.99
        result = product.validate()

        assert result == [
            {
                'property': 'recommended_price_vendor',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 2)" characters.',
            }
        ]

    def test_max_length_error_after_decimal(self, product):
        """Float is too long after decimal point."""
        product.recommended_price_vendor = 99999.101
        result = product.validate()

        assert result == [
            {
                'property': 'recommended_price_vendor',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 2)" characters.',
            }
        ]


class TestProduct3Price:
    """Tests for the price property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.price

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.price = 'xyz'
        result = product.validate()

        assert result == [
            {
                'property': 'price',
                'error': 'TypeError',
                'message': 'The property must be of "float" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.price = 99999.99

        assert not product.validate()

    def test_max_length_error_before_decimal(self, product):
        """Float is too long before decimal point."""
        product.price = 100000.99
        result = product.validate()

        assert result == [
            {
                'property': 'price',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 2)" characters.',
            }
        ]

    def test_max_length_error_after_decimal(self, product):
        """Float is too long after decimal point."""
        product.price = 99999.101
        result = product.validate()

        assert result == [
            {
                'property': 'price',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 2)" characters.',
            }
        ]


class TestProduct3Discountable:
    """Tests for the discountable property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.discountable

    @pytest.mark.parametrize('option', ('J', 'N'))
    def test_set_property(self, product, option):
        """Set the property."""
        product.discountable = option

        assert not product.validate()

    def test_wrong_option(self, product):
        """Is a worng option."""
        product.discountable = 'X'
        result = product.validate()

        assert result == [
            {
                'property': 'discountable',
                'error': 'OptionsError',
                'message': 'Only the options "(\'J\', \'N\')" are allowed.',
            }
        ]

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.discountable = 1
        result = product.validate()

        assert result == [
            {
                'property': 'discountable',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]


class TestProduct3CashDiscountable:
    """Tests for the cash_discountable property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.cash_discountable

    @pytest.mark.parametrize('option', ('J', 'N'))
    def test_set_property(self, product, option):
        """Set the property."""
        product.cash_discountable = option

        assert not product.validate()

    def test_wrong_option(self, product):
        """Is a worng option."""
        product.cash_discountable = 'X'
        result = product.validate()

        assert result == [
            {
                'property': 'cash_discountable',
                'error': 'OptionsError',
                'message': 'Only the options "(\'J\', \'N\')" are allowed.',
            }
        ]

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.cash_discountable = 1
        result = product.validate()

        assert result == [
            {
                'property': 'cash_discountable',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]


class TestProduct3ScaleQuantity1:
    """Tests for the scale_quantity_1 property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.scale_quantity_1

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.scale_quantity_1 = 'xyz'
        result = product.validate()

        assert result == [
            {
                'property': 'scale_quantity_1',
                'error': 'TypeError',
                'message': 'The property must be of "float" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.scale_quantity_1 = 99999.99

        assert not product.validate()

    def test_max_length_error_before_decimal(self, product):
        """Float is too long before decimal point."""
        product.scale_quantity_1 = 100000.99
        result = product.validate()

        assert result == [
            {
                'property': 'scale_quantity_1',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 2)" characters.',
            }
        ]

    def test_max_length_error_after_decimal(self, product):
        """Float is too long after decimal point."""
        product.scale_quantity_1 = 99999.101
        result = product.validate()

        assert result == [
            {
                'property': 'scale_quantity_1',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 2)" characters.',
            }
        ]


class TestProduct3ScaleQuantity1Price:
    """Tests for the scale_quantity_1_price property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.scale_quantity_1_price

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.scale_quantity_1_price = 'xyz'
        result = product.validate()

        assert result == [
            {
                'property': 'scale_quantity_1_price',
                'error': 'TypeError',
                'message': 'The property must be of "float" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.scale_quantity_1_price = 99999.99

        assert not product.validate()

    def test_max_length_error_before_decimal(self, product):
        """Float is too long before decimal point."""
        product.scale_quantity_1_price = 100000.99
        result = product.validate()

        assert result == [
            {
                'property': 'scale_quantity_1_price',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 2)" characters.',
            }
        ]

    def test_max_length_error_after_decimal(self, product):
        """Float is too long after decimal point."""
        product.scale_quantity_1_price = 99999.101
        result = product.validate()

        assert result == [
            {
                'property': 'scale_quantity_1_price',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 2)" characters.',
            }
        ]


class TestProduct3Discountable1:
    """Tests for the discountable_1 property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.discountable_1

    @pytest.mark.parametrize('option', ('J', 'N'))
    def test_set_property(self, product, option):
        """Set the property."""
        product.discountable_1 = option

        assert not product.validate()

    def test_wrong_option(self, product):
        """Is a worng option."""
        product.discountable_1 = 'X'
        result = product.validate()

        assert result == [
            {
                'property': 'discountable_1',
                'error': 'OptionsError',
                'message': 'Only the options "(\'J\', \'N\')" are allowed.',
            }
        ]

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.discountable_1 = 1
        result = product.validate()

        assert result == [
            {
                'property': 'discountable_1',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]

#


class TestProduct3CashDiscountable1:
    """Tests for the cash_discountable_1 property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.cash_discountable_1

    @pytest.mark.parametrize('option', ('J', 'N'))
    def test_set_property(self, product, option):
        """Set the property."""
        product.cash_discountable_1 = option

        assert not product.validate()

    def test_wrong_option(self, product):
        """Is a worng option."""
        product.cash_discountable_1 = 'X'
        result = product.validate()

        assert result == [
            {
                'property': 'cash_discountable_1',
                'error': 'OptionsError',
                'message': 'Only the options "(\'J\', \'N\')" are allowed.',
            }
        ]

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.cash_discountable_1 = 1
        result = product.validate()

        assert result == [
            {
                'property': 'cash_discountable_1',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]


class TestProduct3ScaleQuantity2:
    """Tests for the scale_quantity_2 property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.scale_quantity_2

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.scale_quantity_2 = 'xyz'
        result = product.validate()

        assert result == [
            {
                'property': 'scale_quantity_2',
                'error': 'TypeError',
                'message': 'The property must be of "float" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.scale_quantity_2 = 99999.99

        assert not product.validate()

    def test_max_length_error_before_decimal(self, product):
        """Float is too long before decimal point."""
        product.scale_quantity_2 = 100000.99
        result = product.validate()

        assert result == [
            {
                'property': 'scale_quantity_2',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 2)" characters.',
            }
        ]

    def test_max_length_error_after_decimal(self, product):
        """Float is too long after decimal point."""
        product.scale_quantity_2 = 99999.101
        result = product.validate()

        assert result == [
            {
                'property': 'scale_quantity_2',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 2)" characters.',
            }
        ]


class TestProduct3ScaleQuantity2Price:
    """Tests for the scale_quantity_2_price property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.scale_quantity_2_price

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.scale_quantity_2_price = 'xyz'
        result = product.validate()

        assert result == [
            {
                'property': 'scale_quantity_2_price',
                'error': 'TypeError',
                'message': 'The property must be of "float" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.scale_quantity_2_price = 99999.99

        assert not product.validate()

    def test_max_length_error_before_decimal(self, product):
        """Float is too long before decimal point."""
        product.scale_quantity_2_price = 100000.99
        result = product.validate()

        assert result == [
            {
                'property': 'scale_quantity_2_price',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 2)" characters.',
            }
        ]

    def test_max_length_error_after_decimal(self, product):
        """Float is too long after decimal point."""
        product.scale_quantity_2_price = 99999.101
        result = product.validate()

        assert result == [
            {
                'property': 'scale_quantity_2_price',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 2)" characters.',
            }
        ]


class TestProduct3Discountable2:
    """Tests for the discountable_2 property."""

    def test_check_default(self, product):
        """Check the default property."""
        product = Product3()

        assert not product.discountable_2

    @pytest.mark.parametrize('option', ('J', 'N'))
    def test_set_property(self, product, option):
        """Set the property."""
        product.discountable_2 = option

        assert not product.validate()

    def test_wrong_option(self, product):
        """Is a worng option."""
        product.discountable_2 = 'X'
        result = product.validate()

        assert result == [
            {
                'property': 'discountable_2',
                'error': 'OptionsError',
                'message': 'Only the options "(\'J\', \'N\')" are allowed.',
            }
        ]

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.discountable_2 = 1
        result = product.validate()

        assert result == [
            {
                'property': 'discountable_2',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]


class TestProduct3CashDiscountable2:
    """Tests for the cash_discountable_2 property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.cash_discountable_2

    @pytest.mark.parametrize('option', ('J', 'N'))
    def test_set_property(self, product, option):
        """Set the property."""
        product.cash_discountable_2 = option

        assert not product.validate()

    def test_wrong_option(self, product):
        """Is a worng option."""
        product.cash_discountable_2 = 'X'
        result = product.validate()

        assert result == [
            {
                'property': 'cash_discountable_2',
                'error': 'OptionsError',
                'message': 'Only the options "(\'J\', \'N\')" are allowed.',
            }
        ]

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.cash_discountable_2 = 1
        result = product.validate()

        assert result == [
            {
                'property': 'cash_discountable_2',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]


class TestProduct3ScaleQuantity3:
    """Tests for the scale_quantity_3 property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.scale_quantity_3

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.scale_quantity_3 = 'xyz'
        result = product.validate()

        assert result == [
            {
                'property': 'scale_quantity_3',
                'error': 'TypeError',
                'message': 'The property must be of "float" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.scale_quantity_3 = 99999.99

        assert not product.validate()

    def test_max_length_error_before_decimal(self, product):
        """Float is too long before decimal point."""
        product.scale_quantity_3 = 100000.99
        result = product.validate()

        assert result == [
            {
                'property': 'scale_quantity_3',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 2)" characters.',
            }
        ]

    def test_max_length_error_after_decimal(self, product):
        """Float is too long after decimal point."""
        product.scale_quantity_3 = 99999.101
        result = product.validate()

        assert result == [
            {
                'property': 'scale_quantity_3',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 2)" characters.',
            }
        ]


class TestProduct3ScaleQuantity3Price:
    """Tests for the scale_quantity_3_price property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.scale_quantity_3_price

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.scale_quantity_3_price = 'xyz'
        result = product.validate()

        assert result == [
            {
                'property': 'scale_quantity_3_price',
                'error': 'TypeError',
                'message': 'The property must be of "float" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.scale_quantity_3_price = 99999.99

        assert not product.validate()

    def test_max_length_error_before_decimal(self, product):
        """Float is too long before decimal point."""
        product.scale_quantity_3_price = 100000.99
        result = product.validate()

        assert result == [
            {
                'property': 'scale_quantity_3_price',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 2)" characters.',
            }
        ]

    def test_max_length_error_after_decimal(self, product):
        """Float is too long after decimal point."""
        product.scale_quantity_3_price = 99999.101
        result = product.validate()

        assert result == [
            {
                'property': 'scale_quantity_3_price',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 2)" characters.',
            }
        ]


class TestProduct3Discountable3:
    """Tests for the discountable_3 property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.discountable_3

    @pytest.mark.parametrize('option', ('J', 'N'))
    def test_set_property(self, product, option):
        """Set the property."""
        product.discountable_3 = option

        assert not product.validate()

    def test_wrong_option(self, product):
        """Is a worng option."""
        product.discountable_3 = 'X'
        result = product.validate()

        assert result == [
            {
                'property': 'discountable_3',
                'error': 'OptionsError',
                'message': 'Only the options "(\'J\', \'N\')" are allowed.',
            }
        ]

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.discountable_3 = 1
        result = product.validate()

        assert result == [
            {
                'property': 'discountable_3',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]


class TestProduct3CashDiscountable3:
    """Tests for the cash_discountable_3 property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.cash_discountable_3

    @pytest.mark.parametrize('option', ('J', 'N'))
    def test_set_property(self, product, option):
        """Set the property."""
        product.cash_discountable_3 = option

        assert not product.validate()

    def test_wrong_option(self, product):
        """Is a worng option."""
        product.cash_discountable_3 = 'X'
        result = product.validate()

        assert result == [
            {
                'property': 'cash_discountable_3',
                'error': 'OptionsError',
                'message': 'Only the options "(\'J\', \'N\')" are allowed.',
            }
        ]

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.cash_discountable_3 = 1
        result = product.validate()

        assert result == [
            {
                'property': 'cash_discountable_3',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]


class TestProduct3ScaleQuantity4:
    """Tests for the scale_quantity_4 property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.scale_quantity_4

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.scale_quantity_4 = 'xyz'
        result = product.validate()

        assert result == [
            {
                'property': 'scale_quantity_4',
                'error': 'TypeError',
                'message': 'The property must be of "float" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.scale_quantity_4 = 99999.99

        assert not product.validate()

    def test_max_length_error_before_decimal(self, product):
        """Float is too long before decimal point."""
        product.scale_quantity_4 = 100000.99
        result = product.validate()

        assert result == [
            {
                'property': 'scale_quantity_4',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 2)" characters.',
            }
        ]

    def test_max_length_error_after_decimal(self, product):
        """Float is too long after decimal point."""
        product.scale_quantity_4 = 99999.101
        result = product.validate()

        assert result == [
            {
                'property': 'scale_quantity_4',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 2)" characters.',
            }
        ]


class TestProduct3ScaleQuantity4Price:
    """Tests for the scale_quantity_4_price property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.scale_quantity_4_price

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.scale_quantity_4_price = 'xyz'
        result = product.validate()

        assert result == [
            {
                'property': 'scale_quantity_4_price',
                'error': 'TypeError',
                'message': 'The property must be of "float" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.scale_quantity_4_price = 99999.99

        assert not product.validate()

    def test_max_length_error_before_decimal(self, product):
        """Float is too long before decimal point."""
        product.scale_quantity_4_price = 100000.99
        result = product.validate()

        assert result == [
            {
                'property': 'scale_quantity_4_price',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 2)" characters.',
            }
        ]

    def test_max_length_error_after_decimal(self, product):
        """Float is too long after decimal point."""
        product.scale_quantity_4_price = 99999.101
        result = product.validate()

        assert result == [
            {
                'property': 'scale_quantity_4_price',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 2)" characters.',
            }
        ]


class TestProduct3Discountable4:
    """Tests for the discountable_4 property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.discountable_4

    @pytest.mark.parametrize('option', ('J', 'N'))
    def test_set_property(self, product, option):
        """Set the property."""
        product.discountable_4 = option

        assert not product.validate()

    def test_wrong_option(self, product):
        """Is a worng option."""
        product.discountable_4 = 'X'
        result = product.validate()

        assert result == [
            {
                'property': 'discountable_4',
                'error': 'OptionsError',
                'message': 'Only the options "(\'J\', \'N\')" are allowed.',
            }
        ]

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.discountable_4 = 1
        result = product.validate()

        assert result == [
            {
                'property': 'discountable_4',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]


class TestProduct3CashDiscountable4:
    """Tests for the cash_discountable_4 property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.cash_discountable_4

    @pytest.mark.parametrize('option', ('J', 'N'))
    def test_set_property(self, product, option):
        """Set the property."""
        product.cash_discountable_4 = option

        assert not product.validate()

    def test_wrong_option(self, product):
        """Is a worng option."""
        product.cash_discountable_4 = 'X'
        result = product.validate()

        assert result == [
            {
                'property': 'cash_discountable_4',
                'error': 'OptionsError',
                'message': 'Only the options "(\'J\', \'N\')" are allowed.',
            }
        ]

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.cash_discountable_4 = 1
        result = product.validate()

        assert result == [
            {
                'property': 'cash_discountable_4',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]


class TestProduct3ScaleQuantity5:
    """Tests for the scale_quantity_5 property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.scale_quantity_5

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.scale_quantity_5 = 'xyz'
        result = product.validate()

        assert result == [
            {
                'property': 'scale_quantity_5',
                'error': 'TypeError',
                'message': 'The property must be of "float" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.scale_quantity_5 = 99999.99

        assert not product.validate()

    def test_max_length_error_before_decimal(self, product):
        """Float is too long before decimal point."""
        product.scale_quantity_5 = 100000.99
        result = product.validate()

        assert result == [
            {
                'property': 'scale_quantity_5',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 2)" characters.',
            }
        ]

    def test_max_length_error_after_decimal(self, product):
        """Float is too long after decimal point."""
        product.scale_quantity_5 = 99999.101
        result = product.validate()

        assert result == [
            {
                'property': 'scale_quantity_5',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 2)" characters.',
            }
        ]


class TestProduct3ScaleQuantity5Price:
    """Tests for the scale_quantity_5_price property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.scale_quantity_5_price

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.scale_quantity_5_price = 'xyz'
        result = product.validate()

        assert result == [
            {
                'property': 'scale_quantity_5_price',
                'error': 'TypeError',
                'message': 'The property must be of "float" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.scale_quantity_5_price = 99999.99

        assert not product.validate()

    def test_max_length_error_before_decimal(self, product):
        """Float is too long before decimal point."""
        product.scale_quantity_5_price = 100000.99
        result = product.validate()

        assert result == [
            {
                'property': 'scale_quantity_5_price',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 2)" characters.',
            }
        ]

    def test_max_length_error_after_decimal(self, product):
        """Float is too long after decimal point."""
        product.scale_quantity_5_price = 99999.101
        result = product.validate()

        assert result == [
            {
                'property': 'scale_quantity_5_price',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 2)" characters.',
            }
        ]


class TestProduct3Discountable5:
    """Tests for the discountable_5 property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.discountable_5

    @pytest.mark.parametrize('option', ('J', 'N'))
    def test_set_property(self, product, option):
        """Set the property."""
        product.discountable_5 = option

        assert not product.validate()

    def test_wrong_option(self, product):
        """Is a worng option."""
        product.discountable_5 = 'X'
        result = product.validate()

        assert result == [
            {
                'property': 'discountable_5',
                'error': 'OptionsError',
                'message': 'Only the options "(\'J\', \'N\')" are allowed.',
            }
        ]

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.discountable_5 = 1
        result = product.validate()

        assert result == [
            {
                'property': 'discountable_5',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]


class TestProduct3CashDiscountable5:
    """Tests for the cash_discountable_5 property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.cash_discountable_5

    @pytest.mark.parametrize('option', ('J', 'N'))
    def test_set_property(self, product, option):
        """Set the property."""
        product.cash_discountable_5 = option

        assert not product.validate()

    def test_wrong_option(self, product):
        """Is a worng option."""
        product.cash_discountable_5 = 'X'
        result = product.validate()

        assert result == [
            {
                'property': 'cash_discountable_5',
                'error': 'OptionsError',
                'message': 'Only the options "(\'J\', \'N\')" are allowed.',
            }
        ]

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.cash_discountable_5 = 1
        result = product.validate()

        assert result == [
            {
                'property': 'cash_discountable_5',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]


class TestProduct3ItemType:
    """Tests for the item_type property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.item_type

    @pytest.mark.parametrize('option', ('F', 'T', 'W', 'P', 'A'))
    def test_set_property(self, product, option):
        """Set the property."""
        product.item_type = option

        assert not product.validate()

    def test_wrong_option(self, product):
        """Is a worng option."""
        product.item_type = 'X'
        result = product.validate()

        assert result == [
            {
                'property': 'item_type',
                'error': 'OptionsError',
                'message': 'Only the options '
                '"(\'F\', \'T\', \'W\', \'P\', \'A\')" are allowed.',
            }
        ]

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.item_type = 1
        result = product.validate()

        assert result == [
            {
                'property': 'item_type',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]


class TestProduct3SpecialPrice:
    """Tests for the special_price property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.special_price

    def test_set_property(self, product):
        """Set the property."""
        product.special_price = 'A'

        assert not product.validate()

    def test_wrong_option(self, product):
        """Is a worng option."""
        product.special_price = 'X'
        result = product.validate()

        assert result == [
            {
                'property': 'special_price',
                'error': 'OptionsError',
                'message': 'Only the options "A" are allowed.',
            }
        ]

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.special_price = 1
        result = product.validate()

        assert result == [
            {
                'property': 'special_price',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]


class TestProduct3SpecialPriceStartDate:
    """Tests for the special_price_start_date property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.special_price_start_date

    def test_set_property(self, product):
        """Set the property."""
        product.special_price_start_date = date(2021, 3, 6)

        assert not product.validate()

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.special_price_start_date = 123
        result = product.validate()

        assert result == [
            {
                'property': 'special_price_start_date',
                'error': 'TypeError',
                'message': 'The property must be of "date" type.',
            }
        ]


class TestProduct3SpecialPriceEndDate():
    """Tests for the special_price_end property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.special_price_end_date

    def test_set_property(self, product):
        """Set the property."""
        product.special_price_end_date = date(2021, 3, 6)

        assert not product.validate()

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.special_price_end_date = 123
        result = product.validate()

        assert result == [
            {
                'property': 'special_price_end_date',
                'error': 'TypeError',
                'message': 'The property must be of "date" type.',
            }
        ]


class TestProduct3SpecialPriceRecommendedPrice:
    """Tests for the special_price_recommended_price property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.special_price_recommended_price

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.special_price_recommended_price = 'xyz'
        result = product.validate()

        assert result == [
            {
                'property': 'special_price_recommended_price',
                'error': 'TypeError',
                'message': 'The property must be of "float" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.special_price_recommended_price = 99999.99

        assert not product.validate()

    def test_max_length_error_before_decimal(self, product):
        """Float is too long before decimal point."""
        product.special_price_recommended_price = 100000.99
        result = product.validate()

        assert result == [
            {
                'property': 'special_price_recommended_price',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 2)" characters.',
            }
        ]

    def test_max_length_error_after_decimal(self, product):
        """Float is too long after decimal point."""
        product.special_price_recommended_price = 99999.101
        result = product.validate()

        assert result == [
            {
                'property': 'special_price_recommended_price',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 2)" characters.',
            }
        ]


class TestProduct3BasePriceUnit:
    """Tests for the base_price_unit property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.base_price_unit

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.base_price_unit = 123
        result = product.validate()

        assert result == [
            {
                'property': 'base_price_unit',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.base_price_unit = 'x'*10

        assert not product.validate()

    def test_max_length_error(self, product):
        """String is too long."""
        product.base_price_unit = 'x'*11
        result = product.validate()

        assert result == [
            {
                'property': 'base_price_unit',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "10" characters.',
            }
        ]


class TestProduct3BasePriceFactor:
    """Tests for the base_price_factor property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.base_price_factor

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.base_price_factor = 'xyz'
        result = product.validate()

        assert result == [
            {
                'property': 'base_price_factor',
                'error': 'TypeError',
                'message': 'The property must be of "float" type.',
            }
        ]

    def test_set_property(self, product):
        """Set the property."""
        product.base_price_factor = 99999.999

        assert not product.validate()

    def test_max_length_error_before_decimal(self, product):
        """Float is too long before decimal point."""
        product.base_price_factor = 100000.999
        result = product.validate()

        assert result == [
            {
                'property': 'base_price_factor',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 3)" characters.',
            }
        ]

    def test_max_length_error_after_decimal(self, product):
        """Float is too long after decimal point."""
        product.base_price_factor = 99999.1001
        result = product.validate()

        assert result == [
            {
                'property': 'base_price_factor',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "(5, 3)" characters.',
            }
        ]


class TestProduct3AvailableFrom:
    """Tests for the available_from property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.available_from

    def test_set_property(self, product):
        """Set the property."""
        product.available_from = date(2021, 3, 6)

        assert not product.validate()

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.available_from = 123
        result = product.validate()

        assert result == [
            {
                'property': 'available_from',
                'error': 'TypeError',
                'message': 'The property must be of "date" type.',
            }
        ]


class TestProduct3AvailableUntil:
    """Tests for the available_until property."""

    def test_check_default(self):
        """Check the default property."""
        product = Product3()

        assert not product.available_until

    def test_set_property(self, product):
        """Set the property."""
        product.available_until = date(2021, 3, 6)

        assert not product.validate()

    def test_wrong_type(self, product):
        """Is a worng type."""
        product.available_until = 123
        result = product.validate()

        assert result == [
            {
                'property': 'available_until',
                'error': 'TypeError',
                'message': 'The property must be of "date" type.',
            }
        ]


class TestProduct3Validate:
    """Tests for the validate method."""

    def test_all_errors_found(self, product):
        """All errors were found."""
        product.item_number = None
        product.description = 51*'x'
        product.change_code = 'Y'
        product.ean_shop = 123
        result = product.validate()

        assert result == [
            {
                'property': 'item_number',
                'error': 'RequiredError',
                'message': 'The property is required.',
            },
            {
                'property': 'change_code',
                'error': 'OptionsError',
                'message': 'Only the options '
                '"(\'N\', \'A\', \'X\', \'R\', \'V\', \'W\')" are allowed.',
            },
            {
                'property': 'ean_shop',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            },
            {
                'property': 'description',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "50" characters.',
            }
        ]

    def test_skip_type(self, product):
        """Skip type validation."""
        product.item_number = None
        product.description = 51*'x'
        product.change_code = 'Y'
        product.ean_shop = 123
        result = product.validate(skip_type=True)

        assert result == [
            {
                'property': 'item_number',
                'error': 'RequiredError',
                'message': 'The property is required.',
            },
            {
                'property': 'change_code',
                'error': 'OptionsError',
                'message': 'Only the options '
                '"(\'N\', \'A\', \'X\', \'R\', \'V\', \'W\')" are allowed.',
            },
            {
                'property': 'description',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "50" characters.',
            }
        ]

    def test_skip_options(self, product):
        """Skip the options validation."""
        product.item_number = None
        product.description = 51*'x'
        product.change_code = 'Y'
        product.ean_shop = 123
        result = product.validate(skip_options=True)

        assert result == [
            {
                'property': 'item_number',
                'error': 'RequiredError',
                'message': 'The property is required.',
            },
            {
                'property': 'ean_shop',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            },
            {
                'property': 'description',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "50" characters.',
            }
        ]

    def test_skip_max_length(self, product):
        """Skip the max_length validation."""
        product.item_number = None
        product.description = 51*'x'
        product.change_code = 'Y'
        product.ean_shop = 123
        result = product.validate(skip_max_length=True)

        assert result == [
            {
                'property': 'item_number',
                'error': 'RequiredError',
                'message': 'The property is required.',
            },
            {
                'property': 'change_code',
                'error': 'OptionsError',
                'message': 'Only the options '
                '"(\'N\', \'A\', \'X\', \'R\', \'V\', \'W\')" are allowed.',
            },
            {
                'property': 'ean_shop',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            }
        ]

    def test_skip_required(self, product):
        """Skip required value validation."""
        product.item_number = None
        product.description = 51*'x'
        product.change_code = 'Y'
        product.ean_shop = 123
        result = product.validate(skip_required=True)

        assert result == [
            {
                'property': 'change_code',
                'error': 'OptionsError',
                'message': 'Only the options '
                '"(\'N\', \'A\', \'X\', \'R\', \'V\', \'W\')" are allowed.',
            },
            {
                'property': 'ean_shop',
                'error': 'TypeError',
                'message': 'The property must be of "str" type.',
            },
            {
                'property': 'description',
                'error': 'MaxLengthError',
                'message': 'The maximum length is "50" characters.',
            }
        ]

    def test_no_validation_error_occurred(self, bnn, product):
        """No validation error occurred."""
        bnn.products.add(product)

        result = bnn.validate()

        assert not result
